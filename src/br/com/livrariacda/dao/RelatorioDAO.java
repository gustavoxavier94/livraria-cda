package br.com.livrariacda.dao;

import br.com.livrariacda.relatorio.ClienteQueMaisComprou;
import br.com.livrariacda.relatorio.ClienteQueMaisGastou;
import br.com.livrariacda.relatorio.CompraPorClassificacao;
import br.com.livrariacda.relatorio.EditoraMaisAtiva;
import br.com.livrariacda.relatorio.LivroMaisVendido;
import br.com.livrariacda.relatorio.TransportadoraMaisProdutiva;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class RelatorioDAO extends DefaultDAO {
    
    public List<LivroMaisVendido> livrosMaisVendidos() throws SQLException {
        String sql = "SELECT l.titulo, SUM(c.quantidade) as quantidade " +
                    "FROM livraria.contem c " + 
                    "JOIN livraria.livro l ON c.livro = l.isbn " +
                    "GROUP BY l.titulo " +
                    "ORDER BY 2 DESC " +
                    "LIMIT 5";
        
        List<LivroMaisVendido> resultado = new ArrayList<>();
        
        try (PreparedStatement stmt = this.getConnection().prepareStatement(sql)) {
            try(ResultSet rs = stmt.executeQuery()) {
                while(rs.next()) {
                    LivroMaisVendido livro = new LivroMaisVendido();
                    livro.setTitulo(rs.getString("titulo"));
                    livro.setQuantidade(rs.getInt("quantidade"));
                    
                    resultado.add(livro);
                }
            }
        }
        
        return resultado;
    }
    
    public List<ClienteQueMaisComprou> clientesQueMaisCompraram() throws SQLException {
        String sql = "SELECT    cli.nome||' '||cli.sobrenome as cliente, " +
                    "           SUM(cont.quantidade) as quantidade " +
                    "FROM	livraria.compra comp " +
                    "JOIN	livraria.cliente cli ON comp.cliente = cli.cpf " +
                    "JOIN	livraria.contem cont ON comp.id = cont.compra " +
                    "GROUP	BY cli.nome, cli.sobrenome " +
                    "ORDER	BY 2 DESC " +
                    "LIMIT 	5";
        
        List<ClienteQueMaisComprou> resultado = new ArrayList<>();
        
        try (PreparedStatement stmt = this.getConnection().prepareStatement(sql)) {
            try(ResultSet rs = stmt.executeQuery()) {
                while(rs.next()) {
                    ClienteQueMaisComprou livro = new ClienteQueMaisComprou();
                    livro.setCliente(rs.getString("cliente"));
                    livro.setQuantidade(rs.getInt("quantidade"));
                    
                    resultado.add(livro);
                }
            }
        }
        
        return resultado;
    }
    
    public List<ClienteQueMaisGastou> clientesQueMaisGastaram() throws SQLException {
        String sql = "SELECT	cli.nome||' '||cli.sobrenome as nome, " +
                    "           SUM(l.valor * cont.quantidade) as valor " +
                    "FROM	livraria.compra comp " +
                    "JOIN	livraria.cliente cli ON comp.cliente = cli.cpf " +
                    "JOIN	livraria.contem cont ON comp.id = cont.compra " +
                    "JOIN	livraria.livro l ON cont.livro = l.isbn " +
                    "GROUP	BY cli.nome, cli.sobrenome " +
                    "ORDER	BY 2 DESC " +
                    "LIMIT 	5";
        
        List<ClienteQueMaisGastou> resultado = new ArrayList<>();
        
        try (PreparedStatement stmt = this.getConnection().prepareStatement(sql)) {
            try(ResultSet rs = stmt.executeQuery()) {
                while(rs.next()) {
                    ClienteQueMaisGastou livro = new ClienteQueMaisGastou();
                    livro.setNome(rs.getString("nome"));
                    livro.setValor(rs.getInt("valor"));
                    
                    resultado.add(livro);
                }
            }
        }
        
        return resultado;
    }
    
    public List<CompraPorClassificacao> comprasPorCategoria() throws SQLException {
        String sql = "SELECT	class.nome as classificacao,\n" +
                    "           SUM(cont.quantidade) as quantidade\n" +
                    "FROM       livraria.compra comp\n" +
                    "JOIN	livraria.contem cont ON comp.id = cont.compra\n" +
                    "JOIN	livraria.livro l ON cont.livro = l.isbn\n" +
                    "JOIN	livraria.classificacao_livro class ON l.classificacao = class.id\n" +
                    "GROUP	BY class.nome\n" +
                    "ORDER	BY 2 DESC";
        
        List<CompraPorClassificacao> resultado = new ArrayList<>();
        
        try (PreparedStatement stmt = this.getConnection().prepareStatement(sql)) {
            try(ResultSet rs = stmt.executeQuery()) {
                while(rs.next()) {
                    CompraPorClassificacao livro = new CompraPorClassificacao();
                    livro.setClassificacao(rs.getString("classificacao"));
                    livro.setQuantidade(rs.getInt("quantidade"));
                    
                    resultado.add(livro);
                }
            }
        }
        
        return resultado;
    }
    
    public List<EditoraMaisAtiva> editorasMaisAtivas() throws SQLException {
        String sql = "SELECT	e.nome, COUNT(*) as quantidade\n" +
                    "FROM	livraria.livro l\n" +
                    "JOIN	livraria.editora e ON l.editora = e.cnpj\n" +
                    "GROUP	BY e.nome\n" +
                    "ORDER	BY 2 DESC\n" +
                    "LIMIT	5";
        
        List<EditoraMaisAtiva> resultado = new ArrayList<>();
        
        try (PreparedStatement stmt = this.getConnection().prepareStatement(sql)) {
            try(ResultSet rs = stmt.executeQuery()) {
                while(rs.next()) {
                    EditoraMaisAtiva editora = new EditoraMaisAtiva();
                    editora.setNome(rs.getString("nome"));
                    editora.setQuantidade(rs.getInt("quantidade"));
                    
                    resultado.add(editora);
                }
            }
        }
        
        return resultado;
    }
    
    public List<TransportadoraMaisProdutiva> transportadorasMaisProdutivas() throws SQLException {
        String sql = "SELECT	t.nome,\n" +
                    "           COUNT(*) as quantidade\n" +
                    "FROM	livraria.entrega e\n" +
                    "JOIN	livraria.transportadora t ON e.transportadora = t.cnpj\n" +
                    "GROUP	BY t.nome\n" +
                    "ORDER	BY 2 DESC\n" +
                    "LIMIT 	5";
        
        List<TransportadoraMaisProdutiva> resultado = new ArrayList<>();
        
        try (PreparedStatement stmt = this.getConnection().prepareStatement(sql)) {
            try(ResultSet rs = stmt.executeQuery()) {
                while(rs.next()) {
                    TransportadoraMaisProdutiva transportadora = new TransportadoraMaisProdutiva();
                    transportadora.setNome(rs.getString("nome"));
                    transportadora.setQuantidade(rs.getInt("quantidade"));
                    
                    resultado.add(transportadora);
                }
            }
        }
        
        return resultado;
    }
}
