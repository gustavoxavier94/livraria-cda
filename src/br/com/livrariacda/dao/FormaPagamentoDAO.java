package br.com.livrariacda.dao;

import br.com.livrariacda.model.FormaPagamento;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class FormaPagamentoDAO extends DefaultDAO {

    public void cadastrarFormaPagamento(FormaPagamento formaPagamento) throws SQLException {
        int id = getMaxId();
        formaPagamento.setId(id + 1);
        
        String sql = "INSERT INTO livraria.forma_pagamento (id, nome) VALUES (?,?)";

        try (PreparedStatement stmt = getConnection().prepareStatement(sql)) {
            stmt.setInt(1, formaPagamento.getId());
            stmt.setString(2, formaPagamento.getNome());
            
            stmt.execute();
        }

    }

    public FormaPagamento buscarFormaPagamento(int id) throws SQLException {
        String sql = "SELECT id, nome FROM livraria.forma_pagamento WHERE id = ?";

        FormaPagamento formaPagamento = null;

        try (PreparedStatement stmt = getConnection().prepareStatement(sql)) {
            stmt.setInt(1, id);
            try(ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    formaPagamento = new FormaPagamento();
                    formaPagamento.setId(rs.getInt("id"));
                    formaPagamento.setNome(rs.getString("nome"));
                }
            }
            
        }

        return formaPagamento;

    }

    public List<FormaPagamento> listarFormasPagamento() throws SQLException {
        String sql = "SELECT id, nome FROM livraria.forma_pagamento";
        List<FormaPagamento> fps = new ArrayList<>();

        try (PreparedStatement stmt = getConnection().prepareStatement(sql)) {
            try(ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    FormaPagamento formaPagamento = new FormaPagamento();
                    formaPagamento.setId(rs.getInt("id"));
                    formaPagamento.setNome(rs.getString("nome"));

                    fps.add(formaPagamento);
                }
            }
        }
        return fps;
    }

    public void removerFormaPagamento(int id) throws SQLException {
        String sql = "DELETE FROM livraria.forma_pagamento WHERE (id = ?)";

        try (PreparedStatement stmt = getConnection().prepareStatement(sql)) {
            stmt.setInt(1, id);
            int delLines = stmt.executeUpdate(sql);
            System.out.println("Deleted lines: " + delLines);
        }
    }
    
    private int getMaxId() throws SQLException {
        int id = 0;

        String sql = "SELECT MAX(id) AS max FROM livraria.forma_pagamento";

        try (PreparedStatement stmt = getConnection().prepareStatement(sql)) {
            try(ResultSet rs = stmt.executeQuery()) { 
                while (rs.next()) {
                    id = rs.getInt("max");
                }
            }	
        }

        return id;
    }
}
