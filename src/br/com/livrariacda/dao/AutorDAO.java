package br.com.livrariacda.dao;

import br.com.livrariacda.model.Autor;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AutorDAO extends DefaultDAO {
    
    public void cadastrarAutor(Autor autor) throws SQLException {
        
        int maxId = this.getMaxId();
        autor.setId(maxId + 1);
        
        String sql = "INSERT INTO livraria.autor (id, nome) VALUES (?, ?)";
      
        try (PreparedStatement stmt = getConnection().prepareStatement(sql)) {
	
            stmt.setInt(1, autor.getId());
            stmt.setString(2, autor.getNome());

            stmt.execute();
			
        }
    }
    
    public List<Autor> listarAutores() throws SQLException {
        
        String sql = "SELECT id, nome FROM livraria.autor ORDER BY nome";
        
        List<Autor> autores = new ArrayList<>();
        
        try(PreparedStatement stmt = getConnection().prepareStatement(sql)) {
            try(ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    Autor autor = new Autor();
                    autor.setId(rs.getInt("id"));
                    autor.setNome(rs.getString("nome"));
                    
                    autores.add(autor);
                }
            }
        }
	
        return autores;
    }
    
    private int getMaxId() throws SQLException {
        
        int id = 0;

        String sql = "SELECT MAX(id) AS max FROM livraria.autor";

        try (PreparedStatement stmt = getConnection().prepareStatement(sql)) {
            try(ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    id = rs.getInt("max");
                }
            }  
        }

        return id;
    }
    
}
