package br.com.livrariacda.dao;

import br.com.livrariacda.model.Caixa;
import br.com.livrariacda.model.Funcionario;
import br.com.livrariacda.model.Gerente;
import br.com.livrariacda.model.Vendedor;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class FuncionarioDAO extends DefaultDAO {

    public void cadastrar(Funcionario funcionario) throws SQLException {
        int matricula = getMaxMatricula();
        funcionario.setMatricula(matricula+1);

        String sql = "INSERT INTO livraria.funcionario " +
                "(matricula,nome,sobrenome,sexo,logradouro," +
                "numero,complemento,bairro,cidade,estado)" +
                "VALUES (?,?,?,?,?,?,?,?,?,?)";
      
        try (PreparedStatement stmt = getConnection().prepareStatement(sql)) {
           
           stmt.setInt(1, funcionario.getMatricula());
           stmt.setString(2, funcionario.getNome());
           stmt.setString(3, funcionario.getSobrenome());
           stmt.setInt(4, funcionario.getSexo());
           stmt.setString(5, funcionario.getLogradouro());
           stmt.setInt(6, funcionario.getNumero());
           stmt.setString(7, funcionario.getComplemento());
           stmt.setString(8, funcionario.getBairro());
           stmt.setString(9, funcionario.getCidade());
           stmt.setString(10, funcionario.getEstado());
           
           stmt.execute();
        }
        
        switch (funcionario.getCargo()) {
            case "Gerente":
                new GerenteDAO().cadastrar(funcionario);
                break;
            case "Caixa":
                new CaixaDAO().cadastrar(funcionario);
                break;
            case "Vendedor":
                new VendedorDAO().cadastrar(funcionario);
                break;
            default:
                throw new SQLException("Erro ao salvar cargo");
        }
   }
   
    private int getMaxMatricula() throws SQLException {
        int matricula = 0;

        String sql = "SELECT MAX(matricula) AS max FROM livraria.funcionario";

        try (PreparedStatement stmt = getConnection().prepareStatement(sql)) {
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                matricula = rs.getInt("max");
            }
        } catch (SQLException e) {
            throw new SQLException("Erro ao buscar maxima matricula");
        }

        return matricula;
    }

    public void cadastrarTelefone(Funcionario funci, int ddd, long numero) throws SQLException {
        String sql = "INSERT INTO livraria.telefone_funcionario " +
                "(funcionario, ddd, numero) VALUES (?,?,?)";
        
        try (PreparedStatement stmt = getConnection().prepareStatement(sql)) {
           
           stmt.setInt(1, funci.getMatricula());
           stmt.setInt(2, ddd);
           stmt.setLong(3, numero);
           
           stmt.execute();
        }
    }
    
    public List<Funcionario> getFuncionarios() throws SQLException {
        String sql = "SELECT f.matricula,f.nome,f.sobrenome,f.sexo," +
                "f.logradouro,f.numero,f.complemento,f.bairro,f.cidade," +
                "f.cidade,f.estado," +
                "CASE WHEN g.matricula IS NOT NULL" +
                "   THEN 'Gerente'" +
                "   ELSE CASE WHEN c.matricula IS NOT NULL" +
                "       THEN 'Caixa'" +
                "       ELSE 'Vendedor'" +
                "   END " +
                "END as cargo " +
                "FROM livraria.funcionario f " +
                "LEFT JOIN livraria.gerente g ON f.matricula = g.matricula " +
                "LEFT JOIN livraria.vendedor v ON f.matricula = v.matricula " +
                "LEFT JOIN livraria.caixa c ON f.matricula = c.matricula " +
                "ORDER BY f.matricula";
        
        List<Funcionario> funcionarios = new ArrayList<>();
        
        try (PreparedStatement stmt = getConnection().prepareStatement(sql)) {
            try(ResultSet rs = stmt.executeQuery()) {
                String cargo;
                while (rs.next()) {
                    Funcionario funcionario;

                    cargo = rs.getString("cargo");
                    switch (cargo) {
                        case "Gerente":
                            funcionario = new Gerente();
                            break;
                        case "Caixa":
                            funcionario = new Caixa();
                            break;
                        case "Vendedor":
                            funcionario = new Vendedor();
                            break;
                        default:
                            throw new RuntimeException("Cargo invalido");
                    }

                    funcionario.setMatricula(rs.getInt("matricula"));
                    funcionario.setNome(rs.getString("nome"));
                    funcionario.setSobrenome(rs.getString("sobrenome"));
                    funcionario.setSexo(rs.getInt("sexo"));
                    funcionario.setLogradouro(rs.getString("logradouro"));
                    funcionario.setNumero(rs.getInt("numero"));
                    funcionario.setComplemento(rs.getString("complemento"));
                    funcionario.setBairro(rs.getString("bairro"));
                    funcionario.setCidade(rs.getString("cidade"));
                    funcionario.setEstado(rs.getString("estado"));

                    funcionarios.add(funcionario);
                }
            }

        }
        
        return funcionarios;
    }
    
    public Funcionario buscarFuncionario(int matricula) throws SQLException {
        String sql = "SELECT f.matricula,f.nome,f.sobrenome,f.sexo," +
                "f.logradouro,f.numero,f.complemento,f.bairro,f.cidade," +
                "f.cidade,f.estado," +
                "CASE WHEN g.matricula IS NOT NULL" +
                "   THEN 'Gerente'" +
                "   ELSE CASE WHEN c.matricula IS NOT NULL" +
                "       THEN 'Caixa'" +
                "       ELSE 'Vendedor'" +
                "   END " +
                "END as cargo " +
                "FROM livraria.funcionario f " +
                "LEFT JOIN livraria.gerente g ON f.matricula = g.matricula " +
                "LEFT JOIN livraria.vendedor v ON f.matricula = v.matricula " +
                "LEFT JOIN livraria.caixa c ON f.matricula = c.matricula " +
                "WHERE f.matricula = ? " +
                "ORDER BY f.matricula";
        
        Funcionario funcionario = null;
        
        try (PreparedStatement stmt = getConnection().prepareStatement(sql)) {
            stmt.setInt(1, matricula);
            try(ResultSet rs = stmt.executeQuery()) {
                String cargo;
                while (rs.next()) {
                    cargo = rs.getString("cargo");
                    switch (cargo) {
                        case "Gerente":
                            funcionario = new Gerente();
                            break;
                        case "Caixa":
                            funcionario = new Caixa();
                            break;
                        case "Vendedor":
                            funcionario = new Vendedor();
                            break;
                        default:
                            throw new RuntimeException("Cargo invalido");
                    }

                    funcionario.setMatricula(rs.getInt("matricula"));
                    funcionario.setNome(rs.getString("nome"));
                    funcionario.setSobrenome(rs.getString("sobrenome"));
                    funcionario.setSexo(rs.getInt("sexo"));
                    funcionario.setLogradouro(rs.getString("logradouro"));
                    funcionario.setNumero(rs.getInt("numero"));
                    funcionario.setComplemento(rs.getString("complemento"));
                    funcionario.setBairro(rs.getString("bairro"));
                    funcionario.setCidade(rs.getString("cidade"));
                    funcionario.setEstado(rs.getString("estado"));
                }
            }
        }
        
        return funcionario;
    }
    
    public void atualizarFuncionario(Funcionario funcionario) throws SQLException {
        String sql = "UPDATE livraria.funcionario SET nome = ?,"
                + "sobrenome = ?, sexo = ?, logradouro = ?,"
                + "numero = ?, complemento = ?, bairro = ?,"
                + "cidade = ?, estado = ? WHERE matricula = ?";
        
        try (PreparedStatement stmt = getConnection().prepareStatement(sql)) {
            
           stmt.setString(1, funcionario.getNome());
           stmt.setString(2, funcionario.getSobrenome());
           stmt.setInt(3, funcionario.getSexo());
           stmt.setString(4, funcionario.getLogradouro());
           stmt.setInt(5, funcionario.getNumero());
           stmt.setString(6, funcionario.getComplemento());
           stmt.setString(7, funcionario.getBairro());
           stmt.setString(8, funcionario.getCidade());
           stmt.setString(9, funcionario.getEstado());
           stmt.setInt(10, funcionario.getMatricula());
           
           stmt.execute();
        }
    }
}