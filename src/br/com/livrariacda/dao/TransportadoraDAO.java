package br.com.livrariacda.dao;

import br.com.livrariacda.model.Transportadora;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TransportadoraDAO extends DefaultDAO {
    
    public void cadastrarTransportadora(Transportadora transportadora) throws SQLException {

        String sql = "INSERT INTO livraria.transportadora (cnpj, nome) VALUES (?, ?)";
      
        try (PreparedStatement stmt = getConnection().prepareStatement(sql)) {
			
            stmt.setLong(1, transportadora.getCnpj());
            stmt.setString(2, transportadora.getNome());

            stmt.execute();		
        }
   }
	
    public Transportadora buscarTransportadora(long cnpj) throws SQLException {

        String sql = "SELECT cnpj, nome FROM livraria.transportadora WHERE cnpj = ?";
		
        Transportadora transportadora = null;

        try (PreparedStatement stmt = getConnection().prepareStatement(sql)) {
            stmt.setLong(1, cnpj);
            try(ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    transportadora = new Transportadora();
                    transportadora.setCnpj(rs.getLong("cnpj"));
                    transportadora.setNome(rs.getString("nome"));
                }
            }
	
        }
		
        return transportadora;
    }

    public List<Transportadora> listarTransportadoras() throws SQLException {
        String sql = "SELECT cnpj, nome FROM livraria.transportadora";
		
        List<Transportadora> transportadoras = new ArrayList<>();

        try (PreparedStatement stmt = getConnection().prepareStatement(sql)) {	
            try(ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {

                    Transportadora transportadora = new Transportadora();
                    transportadora.setCnpj(rs.getLong("cnpj"));
                    transportadora.setNome(rs.getString("nome"));

                    transportadoras.add(transportadora);
                }
            }
			
            
			
        }
		
        return transportadoras;
    }

    public void removerTransportadora(int cnpj) throws SQLException {
        String sql = "DELETE FROM livraria.transportadora WHERE (cnpj = " + cnpj + ")";

        try (PreparedStatement stmt = getConnection().prepareStatement(sql)) {
			
            int delLines = stmt.executeUpdate(sql);
            System.out.println("Deleted lines: " + delLines);
			
        }
    }
}
