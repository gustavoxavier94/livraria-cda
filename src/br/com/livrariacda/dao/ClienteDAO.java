package br.com.livrariacda.dao;

import br.com.livrariacda.model.Cliente;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ClienteDAO extends DefaultDAO {
    
    public void cadastrarCliente(Cliente cliente) throws SQLException {

        String sql = "INSERT INTO livraria.cliente " +
                "(cpf,nome,sobrenome,sexo,nascimento,logradouro," +
                "numero,complemento,bairro,cidade,estado)" +
                "VALUES (?,?,?,?,?,?,?,?,?,?,?)";
      
        try (PreparedStatement stmt = getConnection().prepareStatement(sql)) {
           
           stmt.setLong(1, cliente.getCpf());
           stmt.setString(2, cliente.getNome());
           stmt.setString(3, cliente.getSobrenome());
           stmt.setInt(4, cliente.getSexo());
           stmt.setDate(5, Date.valueOf(cliente.getNascimento()));
           stmt.setString(6, cliente.getLogradouro());
           stmt.setInt(7, cliente.getNumero());
           stmt.setString(8, cliente.getComplemento());
           stmt.setString(9, cliente.getBairro());
           stmt.setString(10, cliente.getCidade());
           stmt.setString(11, cliente.getEstado());
           
           stmt.execute();
        }
    }
    
    public void cadastrarTelefone(Cliente cliente, int ddd, long numero) throws SQLException {
        String sql = "INSERT INTO livraria.telefone_cliente " +
                "(cliente, ddd, numero) VALUES (?,?,?)";
        
        try (PreparedStatement stmt = getConnection().prepareStatement(sql)) {
           
           stmt.setLong(1, cliente.getCpf());
           stmt.setInt(2, ddd);
           stmt.setLong(3, numero);
           
           stmt.execute();
        }
    }
    
    public List<Cliente> listarClientes() throws SQLException {
        String sql = "SELECT cpf, nome, sobrenome, sexo, nascimento, " +
                "logradouro, numero, complemento, bairro, cidade, estado " +
                "FROM livraria.cliente";
        
        List<Cliente> clientes = new ArrayList<>();
        
        try(PreparedStatement stmt = this.getConnection().prepareStatement(sql)) {
            try(ResultSet rs = stmt.executeQuery()) {
                while(rs.next()) {
                    Cliente cliente = new Cliente();
                    
                    cliente.setCpf(rs.getLong("cpf"));
                    cliente.setNome(rs.getString("nome"));
                    cliente.setSobrenome(rs.getString("sobrenome"));
                    cliente.setSexo(rs.getInt("sexo"));
                    cliente.setNascimento(rs.getDate("nascimento").toLocalDate());
                    cliente.setLogradouro(rs.getString("logradouro"));
                    cliente.setNumero(rs.getInt("numero"));
                    cliente.setComplemento(rs.getString("complemento"));
                    cliente.setBairro(rs.getString("bairro"));
                    cliente.setCidade(rs.getString("cidade"));
                    cliente.setEstado(rs.getString("estado"));
                    
                    clientes.add(cliente);
                }
            }
        }
        
        return clientes;
    }
    
    public Cliente buscarCliente(long cpf) throws SQLException {
       String sql = "SELECT cpf, nome, sobrenome, sexo, nascimento, " +
               "logradouro, numero, complemento, bairro, cidade, estado " +
               "FROM livraria.cliente " +
               "WHERE cpf = ?";
       
       Cliente cliente = new Cliente();
       
       try(PreparedStatement stmt = this.getConnection().prepareStatement(sql)) {
           stmt.setLong(1, cpf);
           try(ResultSet rs = stmt.executeQuery()) {
               while (rs.next()) {
                   cliente.setCpf(rs.getLong("cpf"));
                   cliente.setNome(rs.getString("nome"));
                   cliente.setSobrenome(rs.getString("sobrenome"));
                   cliente.setSexo(rs.getInt("sexo"));
                   cliente.setNascimento(rs.getDate("nascimento").toLocalDate());
                   cliente.setLogradouro(rs.getString("logradouro"));
                   cliente.setNumero(rs.getInt("numero"));
                   cliente.setComplemento(rs.getString("complemento"));
                   cliente.setBairro(rs.getString("bairro"));
                   cliente.setCidade(rs.getString("cidade"));
                   cliente.setEstado(rs.getString("estado"));
               }
           }
       }
       
       return cliente;
    }
    
    public void atualizarCliente(Cliente cliente) throws SQLException {
        String sql = "UPDATE livraria.cliente SET nome = ?,"
                + "sobrenome = ?, sexo = ?, logradouro = ?,"
                + "numero = ?, complemento = ?, bairro = ?,"
                + "cidade = ?, estado = ? WHERE cpf = ?";
        
        try (PreparedStatement stmt = getConnection().prepareStatement(sql)) {
            
           stmt.setString(1, cliente.getNome());
           stmt.setString(2, cliente.getSobrenome());
           stmt.setInt(3, cliente.getSexo());
           stmt.setString(4, cliente.getLogradouro());
           stmt.setInt(5, cliente.getNumero());
           stmt.setString(6, cliente.getComplemento());
           stmt.setString(7, cliente.getBairro());
           stmt.setString(8, cliente.getCidade());
           stmt.setString(9, cliente.getEstado());
           stmt.setLong(10, cliente.getCpf());
           
           stmt.execute();
        }
    }
}
