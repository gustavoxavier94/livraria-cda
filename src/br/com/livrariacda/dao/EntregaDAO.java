package br.com.livrariacda.dao;

import br.com.livrariacda.model.Entrega;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class EntregaDAO extends DefaultDAO {
    
    public void cadastrarEntrega(Entrega entrega) throws SQLException {

        String sql = "INSERT INTO livraria.entrega (compra, transportadora, " +
                "rastreio, frete, data) VALUES (?,?,?,?,?)";
      
        try (PreparedStatement stmt = getConnection().prepareStatement(sql)) {
           stmt.setInt(1, entrega.getCompra());
           stmt.setLong(2, entrega.getTransportadora());
           stmt.setInt(3, entrega.getRastreio());
           stmt.setDouble(4, entrega.getFrete());
           stmt.setDate(5, Date.valueOf(entrega.getData()));
           
           stmt.execute();
       }
    }
    
    public boolean compraEntregue(int idCompra) throws SQLException {
        String sql = "SELECT EXISTS(SELECT compra FROM livraria.entrega WHERE compra = ?)";
        boolean entregue = false;
        
        try (PreparedStatement stmt = getConnection().prepareStatement(sql)) {
            stmt.setInt(1, idCompra);
            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    entregue = rs.getBoolean("exists");
                }
            }
        }
        
        return entregue;
    }
}
