package br.com.livrariacda.dao;

import br.com.livrariacda.model.Livro;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class LivroDAO extends DefaultDAO {
    
   public void cadastrarLivro(Livro livro) throws SQLException {

        String sql = "INSERT INTO livraria.livro (isbn, titulo, lancamento, " +
                "edicao, idioma, valor, paginas, classificacao, editora) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
      
        try (PreparedStatement stmt = getConnection().prepareStatement(sql)) {
			
            stmt.setLong(1, livro.getIsbn());
            stmt.setString(2, livro.getTitulo());
            stmt.setDate(3, Date.valueOf(livro.getLancamento()));
            stmt.setInt(4, livro.getEdicao());
            stmt.setInt(5, livro.getIdioma().getId());
            stmt.setDouble(6, livro.getValor());
            stmt.setInt(7, livro.getPaginas());
            stmt.setInt(8, livro.getClassificacao().getId());
            stmt.setLong(9, livro.getEditora().getCnpj());

            stmt.execute();
			
        }
   }
	
    public Livro buscarLivro(long isbn) throws SQLException {

        String sql = "SELECT isbn, titulo, lancamento, edicao, idioma, valor, " +
                    "paginas, classificacao, editora " +
                    "FROM livraria.livro WHERE isbn = ?";
		
        Livro livro =  new Livro();

        try (PreparedStatement stmt = getConnection().prepareStatement(sql)) {
            
            stmt.setLong(1, isbn);
            try(ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    livro.setIsbn(rs.getLong("isbn"));
                    livro.setTitulo(rs.getString("titulo"));
                    livro.setLancamento(rs.getDate("lancamento").toLocalDate());
                    livro.setEdicao(rs.getInt("edicao"));
                    livro.setIdioma(new IdiomaDAO().buscarIdioma(rs.getInt("idioma")));
                    livro.setValor(rs.getDouble("valor"));
                    livro.setPaginas(rs.getInt("paginas"));
                    livro.setClassificacao(new ClassificacaoDAO().buscarClassificao(rs.getInt("classificacao")));
                    livro.setEditora(new EditoraDAO().buscarEditora(rs.getLong("editora")));
                }
            }
        }
		
        return livro;
    }

    public List<Livro> listarLivros() throws SQLException {
        
        String sql = "SELECT isbn, titulo, lancamento, edicao, idioma, " +
                "valor, paginas, classificacao, editora " + 
                "FROM livraria.livro ORDER BY titulo";
		
        List<Livro> livros = new ArrayList<>();

        try (PreparedStatement stmt = getConnection().prepareStatement(sql)) {
			
            try(ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    
                    Livro livro = new Livro();
                    livro.setIsbn(rs.getLong("isbn"));
                    livro.setTitulo(rs.getString("titulo"));
                    livro.setLancamento(rs.getDate("lancamento").toLocalDate());
                    livro.setEdicao(rs.getInt("edicao"));
                    livro.setIdioma(new IdiomaDAO().buscarIdioma(rs.getInt("idioma")));
                    livro.setValor(rs.getDouble("valor"));
                    livro.setPaginas(rs.getInt("paginas"));
                    livro.setClassificacao(new ClassificacaoDAO().buscarClassificao(rs.getInt("classificacao")));
                    livro.setEditora(new EditoraDAO().buscarEditora(rs.getLong("editora")));

                    livros.add(livro);
                }
            }		
        }
		
        return livros;
    }

    public void removerLivro(long isbn) throws SQLException {
        
        String sql = "DELETE FROM livraria.livro WHERE isbn = ?";

        try (PreparedStatement stmt = getConnection().prepareStatement(sql)) {
            stmt.setLong(1, isbn);
            int delLines = stmt.executeUpdate(sql);
            System.out.println("Deleted lines: " + delLines);	
        }
    }
    
    public void incluirAutor(long isbn, int idAutor) throws SQLException {
        
        String sql = "INSERT INTO livraria.escreve (livro, autor) VALUES (?, ?)";
      
        try (PreparedStatement stmt = getConnection().prepareStatement(sql)) {
			
            stmt.setLong(1, isbn);
            stmt.setInt(2, idAutor);

            stmt.execute();	
        }
    }
}
