package br.com.livrariacda.dao;

import br.com.livrariacda.model.Idioma;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class IdiomaDAO extends DefaultDAO {
    
    public void cadastrarIdioma(Idioma idioma) throws SQLException {
        int id = getMaxId();
        idioma.setId(id + 1);

        String sql = "INSERT INTO livraria.idioma (id, nome) VALUES (?, ?)";
      
        try (PreparedStatement stmt = getConnection().prepareStatement(sql)) {
           stmt.setInt(1, idioma.getId());
           stmt.setString(2, idioma.getNome());
           
           stmt.execute();
       }
    }
    
    private int getMaxId() throws SQLException {
        int id = 0;

        String sql = "SELECT MAX(id) AS max FROM livraria.idioma";

        try (PreparedStatement stmt = getConnection().prepareStatement(sql)) {
            try(ResultSet rs = stmt.executeQuery()) { 
                while (rs.next()) {
                    id = rs.getInt("max");
                }
            }	
        }

        return id;
    }
    
    public Idioma buscarIdioma(int id) throws SQLException {
        String sql = "SELECT id, nome FROM livraria.idioma " +
                    "WHERE id = ?";
        
        Idioma idioma = new Idioma();
        
        try (PreparedStatement stmt = this.getConnection().prepareStatement(sql)) {
            stmt.setInt(1, id);
            try (ResultSet rs = stmt.executeQuery()) {  
                while (rs.next()) {
                    idioma.setId(rs.getInt("id"));
                    idioma.setNome(rs.getString("nome"));
                }
            }
        }
        
        return idioma;
    }
    
    public List<Idioma> listarIdiomas() throws SQLException {
        
        String sql = "SELECT id, nome FROM livraria.idioma ORDER BY nome";

        List<Idioma> idiomas = new ArrayList<>();
        
        try (PreparedStatement stmt = this.getConnection().prepareStatement(sql)) {
            try(ResultSet rs = stmt.executeQuery()) {
                while(rs.next()) {
                    Idioma idioma = new Idioma();
                    idioma.setId(rs.getInt("id"));
                    idioma.setNome(rs.getString("nome"));
                    
                    idiomas.add(idioma);
                }
            }
        }
        
        return idiomas;
    }
    
}
