package br.com.livrariacda.dao;

import br.com.livrariacda.model.Editora;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class EditoraDAO extends DefaultDAO {

    public void cadastrarEditora(Editora editora) throws SQLException {

        String sql = "INSERT INTO livraria.editora (cnpj, nome) VALUES (?, ?)";
      
        try (PreparedStatement stmt = getConnection().prepareStatement(sql)) {	
            stmt.setLong(1, editora.getCnpj());
            stmt.setString(2, editora.getNome());

            stmt.execute();	
        }
    }
	
    public Editora buscarEditora(long cnpj) throws SQLException {
        
        String sql = "SELECT cnpj, nome FROM livraria.editora WHERE cnpj = ?";

        Editora editora = new Editora();

        try (PreparedStatement stmt = getConnection().prepareStatement(sql)) {
            stmt.setLong(1, cnpj);
            try(ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    editora = new Editora();
                    editora.setCnpj(rs.getLong("cnpj"));
                    editora.setNome(rs.getString("nome"));
                }
            }
        }

        return editora;
    }

    public List<Editora> listarEditoras() throws SQLException {
        
        String sql = "SELECT cnpj, nome FROM livraria.editora ORDER BY nome";
		
        List<Editora> editoras = new ArrayList<>();

        try (PreparedStatement stmt = getConnection().prepareStatement(sql)) {	
            try(ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    Editora editora = new Editora();
                    editora.setCnpj(rs.getLong("cnpj"));
                    editora.setNome(rs.getString("nome"));

                    editoras.add(editora);
                }
            }
        }
		
        return editoras;
    }

    public void removerEditora(int cnpj) throws SQLException {
        
        String sql = "DELETE FROM livraria.editora WHERE (cnpj = " + cnpj + ")";

        try (PreparedStatement stmt = getConnection().prepareStatement(sql)) {	
            int delLines = stmt.executeUpdate(sql);
            System.out.println("Deleted lines: " + delLines);	
        }
    }
}
