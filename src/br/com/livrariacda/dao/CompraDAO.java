package br.com.livrariacda.dao;

import br.com.livrariacda.model.Caixa;
import br.com.livrariacda.model.Compra;
import br.com.livrariacda.model.Livro;
import br.com.livrariacda.model.Vendedor;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class CompraDAO extends DefaultDAO {

   public void cadastrarCompra(Compra compra) throws SQLException {
        int id = getMaxId();
        compra.setId(id+1);

        String sql = "INSERT INTO livraria.compra (id, cliente, data, " +
                    "forma_pagamento) VALUES (?, ?, ?, ?)";
      
        try (PreparedStatement stmt = getConnection().prepareStatement(sql)) {	
            stmt.setInt(1, compra.getId());
            stmt.setLong(2, compra.getCliente());
            stmt.setDate(3, Date.valueOf(compra.getData()));
            stmt.setInt(4, compra.getFormaDePagamento());

            stmt.execute();	
        }
   }
   
    private int getMaxId() throws SQLException {
        int id = 0;

        String sql = "SELECT MAX(id) AS max FROM livraria.compra";

        try (PreparedStatement stmt = getConnection().prepareStatement(sql)) {
            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    id = rs.getInt("max");
                }
            }		
        }

        return id;
    }
	
    public Compra buscarCompra(int id) throws SQLException {

        String sql = "SELECT id, cliente, data, forma_pagamento FROM " +
                "livraria.compra WHERE id = ?";
		
        Compra compra = new Compra();

        try (PreparedStatement stmt = getConnection().prepareStatement(sql)) {
            stmt.setInt(1, id);
            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    compra = new Compra();
                    compra.setId(rs.getInt("id"));
                    compra.setCliente(rs.getLong("cliente"));
                    compra.setData(rs.getDate("data").toLocalDate());
                    compra.setFormaDePagamento(rs.getInt("forma_pagamento"));
                }
            }	
        }
		
        return compra;
    }

    public List<Compra> listarCompras() throws SQLException {
        String sql = "SELECT id, cliente, data, forma_pagamento "
                + "FROM livraria.compra";
		
        List<Compra> compras = new ArrayList<>();

        try (PreparedStatement stmt = getConnection().prepareStatement(sql)) {
			
            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {	
                    Compra compra = new Compra();
                    compra.setId(rs.getInt("id"));
                    compra.setCliente(rs.getLong("cliente"));
                    compra.setData(rs.getDate("data").toLocalDate());
                    compra.setFormaDePagamento(rs.getInt("forma_pagamento"));

                    compras.add(compra);
                }
            }		
        }
		
        return compras;
    }
    
    public List<Compra> listarCompras(long cpf) throws SQLException {
        String sql = "SELECT id, cliente, data, forma_pagamento "
                + "FROM livraria.compra "
                + "WHERE cliente = ? "
                + "ORDER BY data";
		
        List<Compra> compras = new ArrayList<>();

        try (PreparedStatement stmt = getConnection().prepareStatement(sql)) {
            stmt.setLong(1, cpf);
            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {	
                    Compra compra = new Compra();
                    compra.setId(rs.getInt("id"));
                    compra.setCliente(rs.getLong("cliente"));
                    compra.setData(rs.getDate("data").toLocalDate());
                    compra.setFormaDePagamento(rs.getInt("forma_pagamento"));

                    compras.add(compra);
                }
            }		
        }
		
        return compras;
    }
    
    public void inserirProcessa(Compra compra, Caixa caixa) throws SQLException {
        
        String sql = "INSERT INTO livraria.processa (compra, caixa) VALUES (?,?)";
        
        try (PreparedStatement stmt = this.getConnection().prepareStatement(sql)) {
            stmt.setInt(1, compra.getId());
            stmt.setInt(2, caixa.getMatricula());
            stmt.execute();
        }
    }
    
    public void inserirParticipa(Compra compra, Vendedor vendedor) throws SQLException {
        
        String sql = "INSERT INTO livraria.participa (compra, vendedor) VALUES (?,?)";
        
        try (PreparedStatement stmt = this.getConnection().prepareStatement(sql)) {
            stmt.setInt(1, compra.getId());
            stmt.setInt(2, vendedor.getMatricula());
            stmt.execute();
        }
    }
    
    public void inserirContem(Compra compra, List<Livro> livros) throws SQLException {
        String sql = "INSERT INTO livraria.contem (compra, livro, quantidade) VALUES (?,?,?)";
        
        Map<Long, Integer> livrosMap = new HashMap<>();
        
        livros.forEach((livro) -> {
            if (livrosMap.containsKey(livro.getIsbn())) {
                livrosMap.put(livro.getIsbn(), livrosMap.get(livro.getIsbn()) + 1);
            } else {
                livrosMap.put(livro.getIsbn(), 1);
            }
        });
        
        try (PreparedStatement stmt = this.getConnection().prepareStatement(sql)) {
            for (Entry<Long, Integer> entry : livrosMap.entrySet()) {
                stmt.setInt(1, compra.getId());
                stmt.setLong(2, entry.getKey());
                stmt.setInt(3, entry.getValue());
                
                stmt.execute();
            }
        }
        
    }
    
    public List<Livro> livrosCompra(int idCompra) throws SQLException {
        String sql = "SELECT livro, quantidade FROM livraria.contem WHERE compra = ?";
        
        List<Livro> livros = new ArrayList<>();
        LivroDAO dao = new LivroDAO();
        
        try (PreparedStatement stmt = this.getConnection().prepareStatement(sql)) {
            stmt.setInt(1, idCompra);
            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    long isbn = rs.getLong("livro");
                    int quantidade = rs.getInt("quantidade");
                    Livro livro = dao.buscarLivro(isbn);
                    
                    for (int i = 1; i <= quantidade; i++) {
                        livros.add(livro);
                    }
                    
                }
            }
        }
        
        return livros;
    }
}
