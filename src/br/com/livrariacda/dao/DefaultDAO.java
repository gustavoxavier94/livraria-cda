package br.com.livrariacda.dao;

import br.com.livrariacda.jdbc.ConnectionFactory;
import java.sql.Connection;

public abstract class DefaultDAO {
    
    public Connection getConnection() {
        return new ConnectionFactory().getConnection();
    }
    
}
