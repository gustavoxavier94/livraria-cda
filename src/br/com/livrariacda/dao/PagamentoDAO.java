package br.com.livrariacda.dao;


import br.com.livrariacda.model.Pagamento;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PagamentoDAO extends DefaultDAO {
    
     public void cadastrarPagamento(Pagamento pagamento) throws SQLException {

        String sql = "INSERT INTO livraria.pagamento"+
                     "(funcionario, ano, mes, valor)"+
                     "VALUES(?,?,?,?)";

        try (PreparedStatement stmt = getConnection().prepareStatement(sql)) {
            stmt.setInt(1, pagamento.getMatricula());
            stmt.setInt(2, pagamento.getAno());
            stmt.setInt(3, pagamento.getMes());
            stmt.setDouble(4, pagamento.getValor());
            
            stmt.execute();
        }

    }
    
    public List<Pagamento> listarPagamentos(int matricula) throws SQLException {
        String sql = "SELECT funcionario, mes, ano, valor "
                + "FROM livraria.pagamento "
                + "WHERE funcionario = ? "
                + "ORDER BY ano, mes";
        
        List<Pagamento> pagamentos = new ArrayList<>();

        try (PreparedStatement stmt = getConnection().prepareStatement(sql)) {
            stmt.setInt(1, matricula);
            try(ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    Pagamento pagamento = new Pagamento();
                    pagamento.setMatricula(rs.getInt("funcionario"));
                    pagamento.setAno(rs.getInt("ano"));
                    pagamento.setMes(rs.getInt("mes"));
                    pagamento.setValor(rs.getDouble("valor"));

                    pagamentos.add(pagamento);
                }
            }
            
            stmt.execute();
        }
        
        return pagamentos;
    }
    
}
