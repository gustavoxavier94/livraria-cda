package br.com.livrariacda.dao;

import br.com.livrariacda.model.Classificacao;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ClassificacaoDAO extends DefaultDAO {

    public void cadastrarClasificacao(Classificacao classificacao) throws SQLException {
        int id = getMaxId();
        classificacao.setId(id + 1);

        String sql = "INSERT INTO livraria.classificacao_livro (id, nome) VALUES (?, ?)";
      
        try (PreparedStatement stmt = getConnection().prepareStatement(sql)) {
           stmt.setInt(1, classificacao.getId());
           stmt.setString(2, classificacao.getNome());
           
           stmt.execute();
       }
    }
   
    private int getMaxId() throws SQLException {
        int id = 0;

        String sql = "SELECT MAX(id) AS max FROM livraria.classificacao_livro";

        try (PreparedStatement stmt = getConnection().prepareStatement(sql)) {
            try(ResultSet rs = stmt.executeQuery()) { 
                while (rs.next()) {
                    id = rs.getInt("max");
                }
            }	
        }

        return id;
    }
	
    public Classificacao buscarClassificao(int id) throws SQLException {
        String sql = "SELECT id, nome FROM livraria.classificacao_livro WHERE id = ?";

        Classificacao classificacao = new Classificacao();

        try (PreparedStatement stmt = getConnection().prepareStatement(sql)) {
            stmt.setInt(1, id);
            try(ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    classificacao = new Classificacao();
                    classificacao.setId(rs.getInt("id"));
                    classificacao.setNome(rs.getString("nome"));
                }
            }
        }
        return classificacao;
    }

    public List<Classificacao> listarClassificacoes() throws SQLException {
        String sql = "SELECT id, nome FROM livraria.classificacao_livro ORDER BY nome";
        List<Classificacao> classificacoes = new ArrayList<>();

        try (PreparedStatement stmt = getConnection().prepareStatement(sql)) {
            try(ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    Classificacao classificacao = new Classificacao();
                    classificacao.setId(rs.getInt("id"));
                    classificacao.setNome(rs.getString("nome"));

                    classificacoes.add(classificacao);
                }
            }	
        }
		
        return classificacoes;
    }

    public void removerClassificacao(int id) throws SQLException {
        String sql = "DELETE FROM livraria.classificacao_livro WHERE (id = " + id + ")";

        try (PreparedStatement stmt = getConnection().prepareStatement(sql)) {
			
            int delLines = stmt.executeUpdate(sql);
            System.out.println("Deleted lines: " + delLines);
			
        }
    }
}
