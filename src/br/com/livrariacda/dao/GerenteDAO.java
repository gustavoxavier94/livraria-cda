package br.com.livrariacda.dao;

import br.com.livrariacda.model.Funcionario;
import java.sql.PreparedStatement;
import java.sql.SQLException;


public class GerenteDAO extends DefaultDAO {
    
    public void cadastrar(Funcionario gerente) throws SQLException {
        
        String sql = "INSERT INTO livraria.gerente (matricula) " +
                        "VALUES (?)";
        
        try (PreparedStatement stmt = getConnection().prepareStatement(sql)) {
            stmt.setInt(1, gerente.getMatricula());
            stmt.execute();
        }
    }
    
}
