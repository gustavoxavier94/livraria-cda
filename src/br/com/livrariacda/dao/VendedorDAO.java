package br.com.livrariacda.dao;

import br.com.livrariacda.model.Funcionario;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class VendedorDAO extends DefaultDAO {
    
    public void cadastrar(Funcionario vendedor) throws SQLException {
        
        String sql = "INSERT INTO livraria.vendedor (matricula) " +
                        "VALUES (?)";
        
        try (PreparedStatement stmt = getConnection().prepareStatement(sql)) {
            stmt.setInt(1, vendedor.getMatricula());
            stmt.execute();
        }
    }
    
    public List<Funcionario> listarVendedores() throws SQLException {
        
        String sql = "SELECT matricula FROM livraria.vendedor";
        
        List<Funcionario> vendedores = new ArrayList<>();
        
        FuncionarioDAO dao = new FuncionarioDAO();
        
        try (PreparedStatement stmt = this.getConnection().prepareStatement(sql)) {
            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    Funcionario caixa = dao.buscarFuncionario(rs.getInt("matricula"));
                    vendedores.add(caixa);
                }
            }
        }
        
        return vendedores;
    }
}
