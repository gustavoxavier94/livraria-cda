package br.com.livrariacda.dao;

import br.com.livrariacda.model.Funcionario;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CaixaDAO extends DefaultDAO {
  
    public void cadastrar(Funcionario caixa) throws SQLException {
        
        String sql = "INSERT INTO livraria.caixa (matricula) " +
                        "VALUES (?)";
        
        try (PreparedStatement stmt = this.getConnection().prepareStatement(sql)) {
            stmt.setInt(1, caixa.getMatricula());
            stmt.execute();
        }
    }
    
    public List<Funcionario> listarCaixas() throws SQLException {
        
        String sql = "SELECT matricula FROM livraria.caixa";
        
        List<Funcionario> caixas = new ArrayList<>();
        
        FuncionarioDAO dao = new FuncionarioDAO();
        
        try (PreparedStatement stmt = this.getConnection().prepareStatement(sql)) {
            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    Funcionario caixa = dao.buscarFuncionario(rs.getInt("matricula"));
                    caixas.add(caixa);
                }
            }
        }
        
        return caixas;
    }
    
}
