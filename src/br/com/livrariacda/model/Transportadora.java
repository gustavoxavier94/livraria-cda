package br.com.livrariacda.model;

public class Transportadora {
    
    private long cnpj;
    private String nome;

    public long getCnpj() {
        return cnpj;
    }
	
    public String getNome() {
        return nome;
    }
	
    public void setCnpj(long cnpj) {
        this.cnpj = cnpj;
    }
	
    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public String toString() {
        return this.nome;
    }
}
