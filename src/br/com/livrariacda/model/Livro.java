package br.com.livrariacda.model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Livro {
    
    private long isbn;
    private String titulo;
    private LocalDate lancamento;
    private int edicao;
    private Idioma idioma;
    private double valor;
    private int paginas;
    private Classificacao classificacao;
    private Editora editora;
	
    public long getIsbn() {
        return isbn;
    }
	
    public String getTitulo() {
        return titulo;
    }
	
    public LocalDate getLancamento() {
        return lancamento;
    }
	
    public int getEdicao() {
        return edicao;
    }
	
    public Idioma getIdioma() {
        return idioma;
    }
	
    public double getValor() {
        return valor;
    }
	
    public int getPaginas() {
        return paginas;
    }
	
    public Classificacao getClassificacao() {
        return classificacao;
    }
	
    public Editora getEditora() {
        return editora;
    }
	
    public void setIsbn(long isbn) {
        this.isbn = isbn;
    }
	
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }
	
    public void setLancamento(LocalDate lancamento) {
        this.lancamento = lancamento;
    }
	
    public void setEdicao(int edicao) {
        this.edicao = edicao;
    }
	
    public void setIdioma(Idioma idioma) {
        this.idioma = idioma;
    }
	
    public void setValor(double valor) {
        this.valor = valor;
    }
	
    public void setPaginas(int paginas) {
        this.paginas = paginas;
    }
	
    public void setClassificacao(Classificacao classificacao) {
        this.classificacao = classificacao;
    }
	
    public void setEditora(Editora editora) {
        this.editora = editora;
    }

    public String getValorFormatado() {
        return String.format("%10.2f", this.valor).replace('.', ',');
    }
    
    public String getLancamentoFormatado() {
        return this.lancamento.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
    }
    
}
