package br.com.livrariacda.model;

public abstract class Funcionario {
    
    private int matricula;
    private String nome;
    private String sobrenome;
    private int sexo;
    private String logradouro;
    private int numero;
    private String complemento;
    private String bairro;
    private String cidade;
    private String estado;
    
    public abstract String getCargo();

    public int getMatricula() {
        return matricula;
    }

    public String getNome() {
        return nome;
    }

    public String getSobrenome() {
        return sobrenome;
    }
    
    public String getNomeCompleto() {
        return nome + " " + sobrenome;
    }

    public int getSexo() {
        return sexo;
    }
    
    public String getSexoString() {
        if (sexo == 0) {
            return "Feminino";
        } else {
            return "Masculino";
        }
    }

    public String getLogradouro() {
        return logradouro;
    }

    public int getNumero() {
        return numero;
    }

    public String getComplemento() {
        return complemento;
    }

    public String getBairro() {
        return bairro;
    }

    public String getCidade() {
        return cidade;
    }

    public String getEstado() {
        return estado;
    }

    public void setMatricula(int matricula) {
        this.matricula = matricula;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setSobrenome(String sobrenome) {
        this.sobrenome = sobrenome;
    }

    public void setSexo(int sexo) {
        if (sexo > 1 || sexo < 0) {
                this.sexo = 0;
        } else {
                this.sexo = sexo;
        }
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return this.nome + " " + this.sobrenome;
    }
    
    
}
