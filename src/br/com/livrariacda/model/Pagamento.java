package br.com.livrariacda.model;

public class Pagamento {

    private int matricula;
    private int ano;
    private int mes;
    private double valor;

    public int getMatricula() {
        return matricula;
    }

    public int getAno() {
        return ano;
    }

    public int getMes() {
        return mes;
    }

    public double getValor() {
        return valor;
    }

    public void setMatricula(int matricula) {
        this.matricula = matricula;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
    
    public String getValorFormatado() {
        return String.format("%10.2f", this.valor).replace('.', ',');
    }
}
