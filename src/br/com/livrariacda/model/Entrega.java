package br.com.livrariacda.model;

import java.time.LocalDate;

public class Entrega {
    
    private int compra;
    private long transportadora;
    private int rastreio;
    private double frete;
    private LocalDate data;

    public int getCompra() {
        return compra;
    }

    public void setCompra(int compra) {
        this.compra = compra;
    }

    public long getTransportadora() {
        return transportadora;
    }

    public void setTransportadora(long transportadora) {
        this.transportadora = transportadora;
    }

    public int getRastreio() {
        return rastreio;
    }

    public void setRastreio(int rastreio) {
        this.rastreio = rastreio;
    }

    public double getFrete() {
        return frete;
    }

    public void setFrete(double frete) {
        this.frete = frete;
    }

    public LocalDate getData() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }
    
    
    
}
