package br.com.livrariacda.model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Compra {
    
    private int id;
    private long cliente;
    private LocalDate data;
    private int formaDePagamento;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getCliente() {
        return cliente;
    }

    public void setCliente(long cliente) {
        this.cliente = cliente;
    }

    public LocalDate getData() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public int getFormaDePagamento() {
        return formaDePagamento;
    }

    public void setFormaDePagamento(int formaDePagamento) {
        this.formaDePagamento = formaDePagamento;
    }
    
    public String getDataFormatada() {
        return this.data.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
    }

}
