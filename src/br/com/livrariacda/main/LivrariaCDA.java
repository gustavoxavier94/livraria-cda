package br.com.livrariacda.main;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class LivrariaCDA extends Application {
    
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/br/com/livrariacda/fxml/Index.fxml"));
        
        Scene scene = new Scene(root);
        
        stage.setMaximized(true);
        stage.setTitle("Livraria CDA");
        stage.getIcons().add(new Image("file:cda_logo.png"));
        stage.setScene(scene);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
