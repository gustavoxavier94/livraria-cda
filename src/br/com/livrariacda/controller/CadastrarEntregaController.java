package br.com.livrariacda.controller;

import br.com.livrariacda.dao.CompraDAO;
import br.com.livrariacda.dao.EntregaDAO;
import br.com.livrariacda.dao.TransportadoraDAO;
import br.com.livrariacda.model.Compra;
import br.com.livrariacda.model.Entrega;
import br.com.livrariacda.model.Transportadora;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

public class CadastrarEntregaController extends FormController implements Initializable {
    
    @FXML
    public TextField compraField;
    
    @FXML
    public Text compraMsg;
    
    @FXML
    public ComboBox transportadoraBox;
    
    @FXML
    public Text transportadoraMsg;
    
    @FXML
    public TextField rastreioField;
    
    @FXML
    public Text rastreioMsg;
    
    @FXML
    public TextField freteField;
    
    @FXML
    public Text freteMsg;
    
    @FXML
    public DatePicker dataField;
    
    @FXML
    public Text dataMsg;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        try {
            List<Transportadora> transportadoras = new TransportadoraDAO().listarTransportadoras();
            
            transportadoraBox.getItems().addAll(transportadoras);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        
    }
    
    @FXML
    public void cadastrarEntrega(ActionEvent event) {
        this.limparMensagens();
        boolean erro = false;
        
        Compra compra = new Compra();
        try {
            int idCompra = Integer.parseInt(compraField.getText());
            if (idCompra < 1) {
                mensagemErro(compraMsg, "Codigo da compra invalido");
                erro = true;
            }
            compra = new CompraDAO().buscarCompra(idCompra);
            if (compra.getId() == 0) {
                mensagemErro(compraMsg, "Compra nao encontrada");
                erro = true;
            }
            boolean entregue = new EntregaDAO().compraEntregue(compra.getId());
            if (entregue) {
                mensagemErro(compraMsg, "Compra ja foi entregue");
                erro = true;
            }
        } catch (NumberFormatException e) {
            mensagemErro(compraMsg, "Codigo da compra invalido");
            erro = true;
        } catch (SQLException e) {
            mensagemErro(compraMsg, "Erro ao buscar compra");
            erro = true;
        }
        
        Transportadora transportadora = (Transportadora) transportadoraBox.getValue();
        if (transportadora == null) {
            mensagemErro(transportadoraMsg, "Transportadora invalida");
            erro = true;
        }
        
        int rastreio = 0;
        try {
            rastreio = Integer.parseInt(rastreioField.getText());
        } catch (NumberFormatException e) {
            mensagemErro(rastreioMsg, "Codigo de rastreio invalido");
            erro = true;
        }
        
        double frete = 0.0;
        try {
            frete = Double.parseDouble(freteField.getText());
        } catch (NumberFormatException e) {
            mensagemErro(freteMsg, "Valor do frete invalido");
            erro = true;
        }
        
        LocalDate data = null;
        try {
            String dataString = dataField.getValue().toString();
            data = LocalDate.parse(dataString, this.getDateFormatter());
            if (data.isAfter(LocalDate.now()) || data.isBefore(compra.getData())) {
                mensagemErro(dataMsg, "Data de Entrega invalida");
                erro = true;
            }
        } catch (Exception e) {
            mensagemErro(dataMsg, "Data de Entrega invalida");
            erro = true;
        }
        
        if (!erro) {
            Entrega entrega = new Entrega();
            entrega.setCompra(compra.getId());
            entrega.setTransportadora(transportadora.getCnpj());
            entrega.setFrete(frete);
            entrega.setRastreio(rastreio);
            entrega.setData(data);
            
            try {
                new EntregaDAO().cadastrarEntrega(entrega);
                this.limparForm();
                mensagemSucesso(msg, "Entrega cadastrada com sucesso!");
            } catch (SQLException e) {
                mensagemErro(msg, "Erro ao cadastrar entrega");
            }
        }
    }

    @Override
    public void limparForm() {
        this.compraField.clear();
        this.transportadoraBox.getSelectionModel().clearSelection();
        this.rastreioField.clear();
        this.freteField.clear();
        this.dataField.setValue(null);
    }

    @Override
    public void limparMensagens() {
        this.compraMsg.setVisible(false);
        this.transportadoraMsg.setVisible(false);
        this.rastreioMsg.setVisible(false);
        this.freteMsg.setVisible(false);
        this.dataMsg.setVisible(false);
    }
    
}
