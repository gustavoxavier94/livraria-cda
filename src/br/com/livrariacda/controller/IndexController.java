package br.com.livrariacda.controller;

import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class IndexController {
    
    private final String fxmlPackage = "br/com/livrariacda/fxml";
    
    @FXML
    public void fecharAplicacao(ActionEvent event) {
        System.exit(0);
    }
    
    @FXML
    public void renderCadastroFuncionario(ActionEvent event) {
        this.render("CadastrarFuncionario.fxml", "Cadastrar Funcionario");
    }
    
    @FXML
    public void renderCadastrarCliente(ActionEvent event) {
        this.render("CadastrarCliente.fxml", "Cadastrar Cliente");
    }
    
    @FXML
    public void renderCadastrarAutor(ActionEvent event) {
        this.render("CadastrarAutor.fxml", "Cadastrar Autor");
    }
    
    @FXML
    public void renderCadastrarEditora(ActionEvent event) {
        this.render("CadastrarEditora.fxml", "Cadastrar Editora");
    }
    
    @FXML
    public void renderCadastrarTransportadora(ActionEvent event) {
        this.render("CadastrarTransportadora.fxml", "Cadastrar Transportadora");
    }
    
    @FXML
    public void renderCadastrarClassificacao(ActionEvent event) {
        this.render("CadastrarClassificacao.fxml", "Cadastrar Classificacao de Livro");
    }
    
    @FXML
    public void renderCadastrarFormaPagamento(ActionEvent event) {
        this.render("CadastrarFormaPagamento.fxml", "Cadastrar Forma de Pagamento");
    }
    
    @FXML
    public void renderCadastrarIdioma(ActionEvent event) {
        this.render("CadastrarIdioma.fxml", "Cadastrar Idioma");
    }
    
    @FXML
    public void renderCadastrarLivro(ActionEvent event) {
        this.render("CadastrarLivro.fxml", "Cadastrar Livro");
    }
    
    @FXML
    public void renderCadastrarCompra(ActionEvent event) {
        this.render("CadastrarCompra.fxml", "Cadastrar Compra");
    }
    
    @FXML
    public void renderCadastrarEntrega(ActionEvent event) {
        this.render("CadastrarEntrega.fxml", "Cadastrar Entrega");
    }
    
    @FXML
    public void renderCadastrarPagamento(ActionEvent event) {
        this.render("CadastrarPagamento.fxml", "Cadastrar Pagamento de Funcionario");
    }
    
    @FXML
    public void renderListarLivros(ActionEvent event) {
        this.render("ListarLivros.fxml", "Catalogo de Livros");
    }
    
    @FXML
    public void renderListarFuncionarios(ActionEvent event) {
        this.render("ListarFuncionarios.fxml", "Listar Funcionarios");
    }

    @FXML
    public void renderListarClientes(ActionEvent event) {
        this.render("ListarClientes.fxml", "Listar Clientes");
    }
    
    @FXML
    public void renderListarCompras(ActionEvent event) {
        this.render("ListarCompras.fxml", "Listar Compras");
    }
    
    @FXML
    public void renderMostrarCompra(ActionEvent event) {
        this.render("MostrarCompra.fxml", "Visualizar Compra");
    }
    
    @FXML
    public void renderMostrarCliente(ActionEvent event) {
        this.render("MostrarCliente.fxml", "Visualizar Cliente");
    }
    
    @FXML
    public void renderMostrarFuncionario(ActionEvent event) {
        this.render("MostrarFuncionario.fxml", "Visualizar Funcionario");
    }
    
    @FXML
    public void renderRelatorioLivrosMaisVendidos(ActionEvent event) {
        this.render("RelatorioLivrosMaisVendidos.fxml", "Relatorio - Livros Mais Vendidos");
    }
    
    @FXML
    public void renderRelatorioClientesQueMaisCompraram(ActionEvent event) {
        this.render("RelatorioClientesQueMaisCompraram.fxml", "Relatorio - Clientes Que Mais Compraram");
    }
    
    @FXML
    public void renderRelatorioClientesQueMaisGastaram(ActionEvent event) {
        this.render("RelatorioClientesQueMaisGastaram.fxml", "Relatorio - Clientes Que Mais Gastaram");
    }
    
    @FXML
    public void renderRelatorioComprasPorClassificacao(ActionEvent event) {
        this.render("RelatorioComprasPorClassificacao.fxml", "Relatorio - Compras Por Classificacao");
    }
    
    @FXML
    public void renderRelatorioEditorasMaisAtivas(ActionEvent event) {
        this.render("RelatorioEditorasMaisAtivas.fxml", "Relatorio - Editoras Mais Ativas");
    }
    
    @FXML
    public void renderRelatorioTransportadorasMaisProdutivas(ActionEvent event) {
        this.render("RelatorioTransportadorasMaisProdutivas.fxml", "Relatorio - Transportadoras Mais Produtivas");
    }
    
    @FXML
    private void render(String fxml, String titulo) {
        Parent root;
        
        try {
            root = FXMLLoader.load(getClass().getClassLoader().getResource(fxmlPath(fxml)));
            
            Stage stage = new Stage();
            stage.setTitle(titulo);
            stage.setScene(new Scene(root));
            stage.show();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    
    private String fxmlPath(String fxml) {
        return this.fxmlPackage + "/" + fxml;
    }
    
}
