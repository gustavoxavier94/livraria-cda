package br.com.livrariacda.controller;

import br.com.livrariacda.dao.AutorDAO;
import br.com.livrariacda.model.Autor;
import java.sql.SQLException;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

public class CadastrarAutorController extends FormController {
    
    @FXML
    public TextField nomeField;
    
    @FXML
    public Text nomeMsg;
    
    @FXML
    public void cadastrarAutor() {
        this.limparMensagens();
        
        boolean erro = false;
        
        String nome = nomeField.getText();
        if (nome.length() < 3) {
            this.mensagemErro(nomeMsg, "Nome invalido");
            erro = true;
        }
        
        if (!erro) {
            Autor autor = new Autor();
            autor.setNome(nome);
            
            try {
                new AutorDAO().cadastrarAutor(autor);
                this.limparForm();
                mensagemSucesso(msg, "Autor cadastrado com sucesso!");  
            } catch (SQLException e) {
                mensagemErro(msg, "Erro ao cadastrar autor");
            }
            
        }
    }
    
    @FXML
    @Override
    public void limparForm() {
        nomeField.clear();
    }
    
    @FXML
    @Override
    public void limparMensagens() {
        nomeMsg.setVisible(false);
    }
    
}
