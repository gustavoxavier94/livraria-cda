package br.com.livrariacda.controller;

import br.com.livrariacda.dao.RelatorioDAO;
import br.com.livrariacda.relatorio.LivroMaisVendido;
import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.XYChart;

public class RelatorioLivrosMaisVendidosController implements Initializable {
    
    @FXML
    public BarChart<Number,String> grafico;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.grafico.setTitle("Livros Mais Vendidos");
        this.grafico.getXAxis().setLabel("Livro");
        this.grafico.getYAxis().setLabel("Quantidade");

        try {
            List<LivroMaisVendido> livros = new RelatorioDAO().livrosMaisVendidos();
            
            livros.forEach((livro) -> {
                XYChart.Series series = new XYChart.Series();
                series.setName(livro.getTitulo());
                series.getData().add(new XYChart.Data(livro.getTitulo(), livro.getQuantidade()));
                this.grafico.getData().add(series);
            });

        } catch(SQLException e) {
            throw new RuntimeException(e);
        }
        
        
    }    
    
}
