package br.com.livrariacda.controller;

import br.com.livrariacda.dao.FuncionarioDAO;
import br.com.livrariacda.dao.PagamentoDAO;
import br.com.livrariacda.model.Funcionario;
import br.com.livrariacda.model.Pagamento;
import java.sql.SQLException;
import java.time.LocalDate;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

public class CadastrarPagamentoController extends FormController {
    
    @FXML
    public TextField funcionarioField;
    
    @FXML
    public Text funcionarioMsg;
    
    @FXML
    public TextField mesField;
    
    @FXML
    public Text mesMsg;
    
    @FXML
    public TextField anoField;
    
    @FXML
    public Text anoMsg;
    
    @FXML
    public TextField valorField;
    
    @FXML
    public Text valorMsg;
    
    @FXML
    public void cadastrarPagamento(ActionEvent event) {
        this.limparMensagens();
        boolean erro = false;
        
        Funcionario funcionario = null;
        try {
            int matricula = Integer.parseInt(funcionarioField.getText());
            if (matricula < 1) {
                mensagemErro(funcionarioMsg, "Matricula invalida");
                erro = true;
            }
            funcionario = new FuncionarioDAO().buscarFuncionario(matricula);
            if (funcionario == null) {
                mensagemErro(funcionarioMsg, "Funcionario nao encontrado");
                erro = true;
            }
        } catch (NumberFormatException e) {
            mensagemErro(funcionarioMsg, "Matricula invalida");
            erro = true;
        } catch (SQLException e) {
            mensagemErro(funcionarioMsg, "Erro ao buscar funcionario");
            erro = true;
        }
        
        int mes = 0;
        try {
            mes = Integer.parseInt(mesField.getText());
            if (mes < 1 || mes > 12) {
                mensagemErro(mesMsg, "Mes invalido");
                erro = true;
            }
        } catch (NumberFormatException e) {
            mensagemErro(mesMsg, "Mes invalido");
            erro = true;
        }
        
        int ano = 0;
        try {
            ano = Integer.parseInt(anoField.getText());
            if (ano < LocalDate.now().getYear() - 5 || ano > LocalDate.now().getYear() + 1) {
                mensagemErro(anoMsg, "Ano invalido");
                erro = true;
            }
        } catch (NumberFormatException e) {
            mensagemErro(anoMsg, "Ano invalido");
            erro = true;
        }
        
        double valor = 0.0;
        try {
            valor = Double.parseDouble(valorField.getText());
            if (valor < 1) {
                mensagemErro(valorMsg, "Valor invalido");
                erro = true;
            }
        } catch (NumberFormatException e) {
            mensagemErro(valorMsg, "Valor invalido");
            erro = true;
        }
        
        if (!erro) {
            Pagamento pagamento = new Pagamento();
            
            pagamento.setMatricula(funcionario.getMatricula());
            pagamento.setAno(ano);
            pagamento.setMes(mes);
            pagamento.setValor(valor);
            
            try {
                new PagamentoDAO().cadastrarPagamento(pagamento);
                
                this.limparForm();
                mensagemSucesso(msg, "Pagamento cadastrado com sucesso!");
            } catch (SQLException e) {
                mensagemErro(msg, "Erro ao cadastrar pagamento");
            }
        }
    }

    @Override
    public void limparForm() {
        this.funcionarioField.clear();
        this.anoField.clear();
        this.mesField.clear();
        this.valorField.clear();
    }

    @Override
    public void limparMensagens() {
        this.funcionarioMsg.setVisible(false);
        this.anoMsg.setVisible(false);
        this.mesMsg.setVisible(false);
        this.valorMsg.setVisible(false);
    }

  
}
