package br.com.livrariacda.controller;

import br.com.livrariacda.dao.LivroDAO;
import br.com.livrariacda.model.Livro;
import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class ListarLivrosController implements Initializable {

    @FXML
    TableView<Livro> tabela;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        LivroDAO dao = new LivroDAO();
        
        try {
            List<Livro> livros = dao.listarLivros();
            tabela.getItems().addAll(livros);
            
            TableColumn<Livro,String> isbnCol = new TableColumn<>("ISBN");
            isbnCol.setCellValueFactory(new PropertyValueFactory<>("isbn"));
            tabela.getColumns().add(isbnCol);
            
            TableColumn<Livro,String> tituloCol = new TableColumn<>("Titulo");
            tituloCol.setCellValueFactory(new PropertyValueFactory<>("titulo"));
            tabela.getColumns().add(tituloCol);
            
            TableColumn<Livro,String> lancamentoCol = new TableColumn<>("Data de Lancamento");
            lancamentoCol.setCellValueFactory(new PropertyValueFactory<>("lancamentoFormatado"));
            tabela.getColumns().add(lancamentoCol);
            
            TableColumn<Livro,String> edicaoCol = new TableColumn<>("Edicao");
            edicaoCol.setCellValueFactory(new PropertyValueFactory<>("edicao"));
            tabela.getColumns().add(edicaoCol);
            
            TableColumn<Livro,String> idiomaCol = new TableColumn<>("Idioma");
            idiomaCol.setCellValueFactory(new PropertyValueFactory<>("idioma"));
            tabela.getColumns().add(idiomaCol);
            
            TableColumn<Livro,String> valorCol = new TableColumn<>("Valor (R$)");
            valorCol.setCellValueFactory(new PropertyValueFactory<>("valorFormatado"));
            tabela.getColumns().add(valorCol);
            
            TableColumn<Livro,String> paginasCol = new TableColumn<>("Paginas");
            paginasCol.setCellValueFactory(new PropertyValueFactory<>("paginas"));
            tabela.getColumns().add(paginasCol);
            
            TableColumn<Livro,String> classificacaoCol = new TableColumn<>("Classificacao");
            classificacaoCol.setCellValueFactory(new PropertyValueFactory<>("classificacao"));
            tabela.getColumns().add(classificacaoCol);
            
            TableColumn<Livro,String> editoraCol = new TableColumn<>("Editora");
            editoraCol.setCellValueFactory(new PropertyValueFactory<>("editora"));
            tabela.getColumns().add(editoraCol);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }    
    
}
