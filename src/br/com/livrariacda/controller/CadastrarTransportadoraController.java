package br.com.livrariacda.controller;

import br.com.livrariacda.dao.TransportadoraDAO;
import br.com.livrariacda.model.Transportadora;
import java.sql.SQLException;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

public class CadastrarTransportadoraController extends FormController {
    
    @FXML
    public TextField cnpjField;
    
    @FXML
    public Text cnpjMsg;
    
    @FXML
    public TextField nomeField;
    
    @FXML
    public Text nomeMsg;
    
    @FXML
    public void cadastrarTransportadora() {
        this.limparMensagens();
        boolean erro = false;
        
        long cnpj = 0;
        try {
            cnpj = Long.parseLong(cnpjField.getText());
            if (String.valueOf(cnpj).length() != 14) {
                mensagemErro(cnpjMsg, "CNPJ invalido");
                erro = true;
            }
        } catch (NumberFormatException e) {
            mensagemErro(cnpjMsg, "CNPJ invalido");
            erro = true;
        }
        
        String nome = nomeField.getText();
        if (nome.length() < 3) {
            mensagemErro(nomeMsg, "Nome invalido");
            erro = true;
        }
        
        if (!erro) {
            Transportadora transportadora = new Transportadora();
            transportadora.setCnpj(cnpj);
            transportadora.setNome(nome);
            
            try {
                new TransportadoraDAO().cadastrarTransportadora(transportadora);
                
                this.limparForm();
                mensagemSucesso(msg, "Transportadora cadastrada com sucesso!");
            } catch (SQLException e) {
                mensagemErro(msg, "Erro ao cadastrar transportadora");
            }
            
        }
    }

    @Override
    public void limparForm() {
        cnpjField.clear();
        nomeField.clear();
    }

    @Override
    public void limparMensagens() {
        cnpjMsg.setVisible(false);
        nomeMsg.setVisible(false);
        msg.setVisible(false);
    }
    
}
