package br.com.livrariacda.controller;

import br.com.livrariacda.dao.FuncionarioDAO;
import br.com.livrariacda.model.Caixa;
import br.com.livrariacda.model.Funcionario;
import br.com.livrariacda.model.Gerente;
import br.com.livrariacda.model.Vendedor;
import java.sql.SQLException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;


public class CadastrarFuncionarioController extends FormController {
    
    @FXML
    public TextField nomeField;
    
    @FXML
    public Text nomeMsg;
    
    @FXML
    public TextField sobrenomeField;
    
    @FXML
    public Text sobrenomeMsg;
    
    @FXML
    public ComboBox sexoBox;
    
    @FXML
    public Text sexoMsg;
    
    @FXML
    public ComboBox cargoBox;
    
    @FXML
    public Text cargoMsg;
    
    @FXML
    public TextField logradouroField;
    
    @FXML
    public Text logradouroMsg;
    
    @FXML
    public TextField numeroField;
    
    @FXML
    public Text numeroMsg;
    
    @FXML
    public TextField complementoField;
    
    @FXML
    public TextField bairroField;
    
    @FXML
    public Text bairroMsg;
    
    @FXML
    public TextField cidadeField;
    
    @FXML
    public Text cidadeMsg;
    
    @FXML
    public ComboBox estadoBox;
    
    @FXML
    public Text estadoMsg;
    
    @FXML
    public TextField dddField;
    
    @FXML
    public Text dddMsg;
    
    @FXML
    public TextField telefoneField;
    
    @FXML
    public Text telefoneMsg;
    
    @FXML
    public void cadastrarFuncionario(ActionEvent event) {
        this.limparMensagens();
        
        boolean erro = false;
        
        String nome = nomeField.getText();
        if (nome.length() < 3) {
            mensagemErro(nomeMsg, "Nome invalido");
            erro = true;
        }
        
        String sobrenome = sobrenomeField.getText();
        if (sobrenome.length() < 3) {
            mensagemErro(sobrenomeMsg, "Sobrenome invalido");
            erro = true;
        }
        
        int sexo = 0;
        try {
            String sexoString = sexoBox.getValue().toString();
            if (sexoString.equals("Masculino")) {
                sexo = 1;
            }
        } catch (Exception e) {
            mensagemErro(sexoMsg, "Sexo invalido");
            erro = true;
        }
        
        String cargo = new String();
        try {
            cargo = cargoBox.getValue().toString();
        } catch (Exception e) {
            mensagemErro(cargoMsg, "Cargo invalido");
            erro = true;
        }
        
        String logradouro = logradouroField.getText();
        if (logradouro.length() < 3) {
            mensagemErro(logradouroMsg, "Logradouro invalido");
            erro = true;
        }
        
        int numero = 0;
        try {
            numero = Integer.parseInt(numeroField.getText());
        } catch (NumberFormatException e) {
            mensagemErro(numeroMsg, "Numero invalido");
            erro = true;
        }
        
        String complemento = complementoField.getText();
        
        String bairro = bairroField.getText();
        if (bairro.length() < 3) {
            mensagemErro(bairroMsg, "Bairro invalido");
            erro = true;
        }
        
        String cidade = cidadeField.getText();
        if (cidade.length() < 3) {
            mensagemErro(cidadeMsg, "Cidade invalido");
            erro = true;
        }
        
        String estado = new String();
        try{
            estado = estadoBox.getValue().toString();
            if (estado.length() != 2) {
                mensagemErro(estadoMsg, "Estado invalido");
                erro = true;
            }
        } catch (Exception e) {
            mensagemErro(estadoMsg, "Estado invalido");
            erro = true;
        }
        
        int ddd = 0;
        try {
            ddd = Integer.parseInt(dddField.getText());
            if (ddd < 0 || ddd > 99) {
                mensagemErro(dddMsg, "DDD invalido");
                erro = true;
            }
        } catch (NumberFormatException e) {
            mensagemErro(dddMsg, "DDD invalido");
            erro = true;
        }
        
        long telefone = 0;
        try {
            telefone = Long.parseLong(telefoneField.getText());
            if (telefone < 10000000 || telefone > 999999999) {
                mensagemErro(telefoneMsg, "Telefone invalido");
                erro = true;
            }
        } catch (NumberFormatException e) {
            mensagemErro(telefoneMsg, "Telefone invalido");
            erro = true;
        }
        
        if (!erro) {
            Funcionario funcionario;
            
            switch (cargo) {
                case "Gerente":
                    funcionario = new Gerente();
                    break;
                case "Caixa":
                    funcionario = new Caixa();
                    break;
                case "Vendedor":
                    funcionario = new Vendedor();
                    break;
                default:
                    throw new RuntimeException("Cargo nao identificado");
            }
            
            funcionario.setNome(nome);
            funcionario.setSobrenome(sobrenome);
            funcionario.setSexo(sexo);
            funcionario.setLogradouro(logradouro);
            funcionario.setNumero(numero);
            funcionario.setComplemento(complemento);
            funcionario.setBairro(bairro);
            funcionario.setCidade(cidade);
            funcionario.setEstado(estado);
             
            FuncionarioDAO dao = new FuncionarioDAO();
            
            try {
                dao.cadastrar(funcionario);
                dao.cadastrarTelefone(funcionario, ddd, telefone);
                
                this.limparForm();
                mensagemSucesso(msg, "Funcionario cadastrado com sucesso!");  
            } catch (SQLException e) {
                mensagemErro(msg, "Erro ao cadastrar funcionario");
            }  
        }
    }
    
    @FXML
    @Override
    public void limparForm() {
        nomeField.clear();
        sobrenomeField.clear();
        sexoBox.getSelectionModel().clearSelection();
        cargoBox.getSelectionModel().clearSelection();
        logradouroField.clear();
        numeroField.clear();
        complementoField.clear();
        bairroField.clear();
        cidadeField.clear();
        estadoBox.getSelectionModel().clearSelection();
        dddField.clear();
        telefoneField.clear();
    }
    
    @FXML
    @Override
    public void limparMensagens() {
        nomeMsg.setVisible(false);
        sobrenomeMsg.setVisible(false);
        sexoMsg.setVisible(false);
        cargoMsg.setVisible(false);
        logradouroMsg.setVisible(false);
        numeroMsg.setVisible(false);
        bairroMsg.setVisible(false);
        cidadeMsg.setVisible(false);
        estadoMsg.setVisible(false);
        dddMsg.setVisible(false);
        telefoneMsg.setVisible(false);
        msg.setVisible(false);
    }
    
}
