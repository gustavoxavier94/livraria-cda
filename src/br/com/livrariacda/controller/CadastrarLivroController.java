package br.com.livrariacda.controller;

import br.com.livrariacda.dao.AutorDAO;
import br.com.livrariacda.dao.ClassificacaoDAO;
import br.com.livrariacda.dao.EditoraDAO;
import br.com.livrariacda.dao.IdiomaDAO;
import br.com.livrariacda.dao.LivroDAO;
import br.com.livrariacda.model.Autor;
import br.com.livrariacda.model.Classificacao;
import br.com.livrariacda.model.Editora;
import br.com.livrariacda.model.Idioma;
import br.com.livrariacda.model.Livro;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

public class CadastrarLivroController extends FormController implements Initializable {
    
    @FXML
    public TextField isbnField;
    
    @FXML
    public Text isbnMsg;
    
    @FXML
    public TextField tituloField;
    
    @FXML
    public Text tituloMsg;
    
    @FXML
    public ComboBox autorBox;
    
    @FXML
    public Text autorMsg;
    
    @FXML
    public DatePicker lancamentoField;
    
    @FXML
    public Text lancamentoMsg;
    
    @FXML
    public TextField edicaoField;
    
    @FXML
    public Text edicaoMsg;
    
    @FXML
    public ComboBox idiomaBox;
    
    @FXML
    public Text idiomaMsg;
    
    @FXML
    public TextField valorField;
    
    @FXML
    public Text valorMsg;
    
    @FXML
    public TextField paginasField;
    
    @FXML
    public Text paginasMsg;
    
    @FXML
    public ComboBox classificacaoBox;
    
    @FXML
    public Text classificacaoMsg;
    
    @FXML
    public ComboBox editoraBox;
    
    @FXML
    public Text editoraMsg;
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        List<Autor> autores = new ArrayList<>();
        List<Idioma> idiomas = new ArrayList<>();
        List<Editora> editoras = new ArrayList<>();
        List<Classificacao> classificacoes = new ArrayList<>();
        
        try {
            autores = new AutorDAO().listarAutores();
            idiomas = new IdiomaDAO().listarIdiomas();
            editoras = new EditoraDAO().listarEditoras();
            classificacoes = new ClassificacaoDAO().listarClassificacoes();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        
        this.autorBox.getItems().addAll(autores);
        this.idiomaBox.getItems().addAll(idiomas);
        this.editoraBox.getItems().addAll(editoras);
        this.classificacaoBox.getItems().addAll(classificacoes);
    }
    
    @FXML
    public void cadastrarLivro(ActionEvent event) {
        this.limparMensagens();
        boolean erro = false;
        
        long isbn = 0;
        try {
            isbn = Long.parseLong(this.isbnField.getText());
            if (String.valueOf(isbn).length() != 13) {
                mensagemErro(isbnMsg, "Codigo ISBN invalido");
                erro = true;
            }
        } catch (NumberFormatException e) {
            mensagemErro(isbnMsg, "Codigo ISBN invalido");
            erro = true;
        }
        
        String titulo = tituloField.getText();
        if (titulo.length() < 3) {
            mensagemErro(tituloMsg, "Titulo invalido");
            erro = true;
        }
        
        Autor autor = new Autor();
        try {
            autor = (Autor) autorBox.getValue();
            if (autor == null) {
                mensagemErro(autorMsg, "Autor invalido");
                erro = true;
            }
        } catch (Exception e) {
            mensagemErro(autorMsg, "Autor invalido");
            erro = true;
        }
        
        LocalDate lancamento = null;
        try {
            String lancamentoString = lancamentoField.getValue().toString();
            lancamento = LocalDate.parse(lancamentoString, this.getDateFormatter());
            if (lancamento.isAfter(LocalDate.now())) {
                mensagemErro(lancamentoMsg, "Data de Lancamento invalida");
                erro = true;
            }
        } catch (Exception e) {
            mensagemErro(lancamentoMsg, "Data de Lancamento invalida");
            erro = true;
        }
        
        int edicao = 0;
        try {
            edicao = Integer.parseInt(edicaoField.getText());
            if (edicao < 0) {
                mensagemErro(edicaoMsg, "Edicao invalida");
                erro = true;
            }
        } catch (NumberFormatException e) {
            mensagemErro(edicaoMsg, "Edicao invalida");
            erro = true;
        }
        
        Idioma idioma = new Idioma();
        try {
            idioma = (Idioma) idiomaBox.getValue();
            if (idioma == null) {
                mensagemErro(idiomaMsg, "Idioma invalido");
                erro = true;
            }
        } catch (Exception e) {
            mensagemErro(idiomaMsg, "Idioma invalido");
            erro = true;
        }
        
        double valor = 0.0;
        try {
            valor = Double.parseDouble(valorField.getText());
            if (valor < 0.0) {
                mensagemErro(valorMsg, "Valor invalido");
                erro = true;
            }
        } catch (NumberFormatException e) {
            mensagemErro(valorMsg, "Valor invalido");
            erro = true;
        }
        
        int paginas = 0;
        try {
            paginas = Integer.parseInt(paginasField.getText());
            if (paginas < 1) {
                mensagemErro(paginasMsg, "Numero de paginas invalido");
                erro = true;
            }
        } catch (NumberFormatException e) {
            mensagemErro(paginasMsg, "Numero de paginas invalido");
            erro = true;
        }
        
        Classificacao classificacao = new Classificacao();
        try {
            classificacao = (Classificacao) classificacaoBox.getValue();
            if (classificacao == null) {
                mensagemErro(classificacaoMsg, "Idioma invalido");
                erro = true;
            }
        } catch (Exception e) {
            mensagemErro(classificacaoMsg, "Classificacao invalida");
            erro = true;
        }
        
        Editora editora = new Editora();
        try {
            editora = (Editora) editoraBox.getValue();
            if (editora == null) {
                mensagemErro(editoraMsg, "Editora invalida");
                erro = true;
            }
        } catch (Exception e) {
            mensagemErro(editoraMsg, "Editora invalida");
            erro = true;
        }
        
        if (!erro) {
            Livro livro = new Livro();
            livro.setIsbn(isbn);
            livro.setTitulo(titulo);
            livro.setLancamento(lancamento);
            livro.setEdicao(edicao);
            livro.setIdioma(idioma);
            livro.setValor(valor);
            livro.setPaginas(paginas);
            livro.setClassificacao(classificacao);
            livro.setEditora(editora);
            
            LivroDAO dao = new LivroDAO();
            
            try {
                dao.cadastrarLivro(livro);
                dao.incluirAutor(livro.getIsbn(), autor.getId());
                
                this.limparForm();
                this.mensagemSucesso(msg, "Livro cadastrado com sucesso!");
            } catch (SQLException e) {
                mensagemErro(msg, "Erro ao cadastrar livro");
            }
        }
        
    }

    @Override
    public void limparForm() {
        this.isbnField.clear();
        this.tituloField.clear();
        this.autorBox.getSelectionModel().clearSelection();
        this.lancamentoField.setValue(null);
        this.edicaoField.clear();
        this.idiomaBox.getSelectionModel().clearSelection();
        this.valorField.clear();
        this.paginasField.clear();
        this.classificacaoBox.getSelectionModel().clearSelection();
        this.editoraBox.getSelectionModel().clearSelection();
    }

    @Override
    public void limparMensagens() {
        this.isbnMsg.setVisible(false);
        this.tituloMsg.setVisible(false);
        this.autorMsg.setVisible(false);
        this.lancamentoMsg.setVisible(false);
        this.edicaoMsg.setVisible(false);
        this.idiomaMsg.setVisible(false);
        this.valorMsg.setVisible(false);
        this.paginasMsg.setVisible(false);
        this.classificacaoMsg.setVisible(false);
        this.editoraMsg.setVisible(false);
        this.msg.setVisible(false);
    }  
}
