package br.com.livrariacda.controller;

import javafx.fxml.FXML;
import javafx.scene.text.Text;

public abstract class FormController extends DefaultController {
    
    @FXML
    public Text msg;
    
    @FXML
    public abstract void limparForm();
    
    @FXML
    public abstract void limparMensagens();
    
}
