package br.com.livrariacda.controller;

import br.com.livrariacda.dao.FormaPagamentoDAO;
import br.com.livrariacda.model.FormaPagamento;
import java.sql.SQLException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

public class CadastrarFormaPagamentoController extends FormController {

    @FXML
    public TextField formaPagamentoField;
    
    @FXML
    public Text formaPagamentoMsg;
    
    @FXML
    public void cadastrarFormaPagamento(ActionEvent event) {
        this.limparMensagens();
        
        boolean erro = false;
        
        String nome = formaPagamentoField.getText();
        if (nome.length() < 3) {
            this.mensagemErro(formaPagamentoMsg, "Forma de pagamento invalida");
            erro = true;
        }
        
        if (!erro) {
            try {
                FormaPagamento formaPagamento = new FormaPagamento();
                formaPagamento.setNome(nome);
                new FormaPagamentoDAO().cadastrarFormaPagamento(formaPagamento);
                
                this.limparForm();
                mensagemSucesso(msg, "Forma de pagamento cadastrada com sucesso!");
            } catch (SQLException e) {
                mensagemErro(msg, "Erro ao cadastrar forma de pagamento");
            }
                
        }
    }

    @Override
    public void limparForm() {
        this.formaPagamentoField.clear();
    }

    @Override
    public void limparMensagens() {
        formaPagamentoMsg.setVisible(false);
        msg.setVisible(false);
    }
    
}
