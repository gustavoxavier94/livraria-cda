package br.com.livrariacda.controller;

import br.com.livrariacda.dao.ClienteDAO;
import br.com.livrariacda.model.Cliente;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;


public class MostrarClienteController extends FormController implements Initializable {
    
    @FXML
    public TextField cpfField;
    
    @FXML
    public Text cpfMsg;
    
    @FXML
    public TextField nomeField;
    
    @FXML
    public TextField sobrenomeField;
    
    @FXML
    public TextField logradouroField;
    
    @FXML
    public TextField numeroField;
    
    @FXML
    public TextField complementoField;
    
    @FXML
    public TextField bairroField;
    
    @FXML
    public TextField cidadeField;
    
    @FXML
    public ComboBox estadoBox;
    
    @FXML
    public ComboBox sexoBox;
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        List<String> sexos = new ArrayList<>();
        sexos.add("Feminino");
        sexos.add("Masculino");
        this.sexoBox.getItems().addAll(sexos);
        
        List<String> estados = new ArrayList<>();
        estados.add("AC");
        estados.add("AL");
        estados.add("AP");
        estados.add("AM");
        estados.add("BA");
        estados.add("CE");
        estados.add("DF");
        estados.add("ES");
        estados.add("GO");
        estados.add("MA");
        estados.add("MT");
        estados.add("MS");
        estados.add("MG");
        estados.add("PA");
        estados.add("PB");
        estados.add("PR");
        estados.add("PE");
        estados.add("PI");
        estados.add("RJ");
        estados.add("RN");
        estados.add("RS");
        estados.add("RO");
        estados.add("RR");
        estados.add("SC");
        estados.add("SP");
        estados.add("SE");
        estados.add("TO");
        this.estadoBox.getItems().addAll(estados); 
    }
    
    @FXML
    public void mostrarCliente(ActionEvent event) {
        this.limparMensagens();
        this.limparForm();
        this.desativarCampos();
        
        boolean erro = false;
        
        Cliente cliente = null;
        try {
            long cpf = Long.parseLong(cpfField.getText());
            cliente = new ClienteDAO().buscarCliente(cpf);
            if (cliente == null) {
                mensagemErro(cpfMsg, "Cliente nao encontrado");
                erro = true;
            }
        } catch (NumberFormatException e) {
            mensagemErro(cpfMsg, "CPF invalida");
            erro = true;
        } catch (SQLException e) {
            mensagemErro(cpfMsg, "Erro ao buscar cliente");
            erro = true;
        }
        
        if (!erro) {
            this.ativarCampos();
            this.nomeField.setText(cliente.getNome());
            this.sobrenomeField.setText(cliente.getSobrenome());
            this.logradouroField.setText(cliente.getLogradouro());
            this.numeroField.setText(String.valueOf(cliente.getNumero()));
            this.complementoField.setText(cliente.getComplemento());
            this.bairroField.setText(cliente.getBairro());
            this.cidadeField.setText(cliente.getCidade());
            this.estadoBox.getSelectionModel().select(cliente.getEstado());
            this.sexoBox.getSelectionModel().select(cliente.getSexoString());
        }
    }
    
    @FXML
    public void atualizarCliente(ActionEvent event) {
        this.limparMensagens();
        boolean erro = false;
        
        Cliente cliente = null;
        try {
            long cpf = Long.parseLong(cpfField.getText());
            cliente = new ClienteDAO().buscarCliente(cpf);
            if (cliente == null) {
                mensagemErro(msg, "Cliente nao encontrado");
                erro = true;
            }
        } catch (NumberFormatException e) {
            mensagemErro(msg, "Matricula invalida");
            erro = true;
        } catch (SQLException e) {
            mensagemErro(msg, "Erro ao buscar funcionario");
            erro = true;
        }
        
        String nome = nomeField.getText();
        if (nome.length() < 3) {
            mensagemErro(msg, "Nome invalido");
            erro = true;
        }
        
        String sobrenome = sobrenomeField.getText();
        if (sobrenome.length() < 3) {
            mensagemErro(msg, "Sobrenome invalido");
            erro = true;
        }
        
        int sexo = 0;
        try {
            String sexoString = sexoBox.getValue().toString();
            if (sexoString.equals("Masculino")) {
                sexo = 1;
            }
        } catch (Exception e) {
            mensagemErro(msg, "Sexo invalido");
            erro = true;
        }
        
        String logradouro = logradouroField.getText();
        if (logradouro.length() < 3) {
            mensagemErro(msg, "Logradouro invalido");
            erro = true;
        }
        
        int numero = 0;
        try {
            numero = Integer.parseInt(numeroField.getText());
        } catch (NumberFormatException e) {
            mensagemErro(msg, "Numero invalido");
            erro = true;
        }
        
        String complemento = complementoField.getText();
        
        String bairro = bairroField.getText();
        if (bairro.length() < 3) {
            mensagemErro(msg, "Bairro invalido");
            erro = true;
        }
        
        String cidade = cidadeField.getText();
        if (cidade.length() < 3) {
            mensagemErro(msg, "Cidade invalido");
            erro = true;
        }
        
        String estado = new String();
        try{
            estado = estadoBox.getValue().toString();
            if (estado.length() != 2) {
                mensagemErro(msg, "Estado invalido");
                erro = true;
            }
        } catch (Exception e) {
            mensagemErro(msg, "Estado invalido");
            erro = true;
        }   
        
        if (!erro) {
            cliente.setNome(nome);
            cliente.setSobrenome(sobrenome);
            cliente.setSexo(sexo);
            cliente.setLogradouro(logradouro);
            cliente.setNumero(numero);
            cliente.setComplemento(complemento);
            cliente.setBairro(bairro);
            cliente.setCidade(cidade);
            cliente.setEstado(estado);
            
            try {
                new ClienteDAO().atualizarCliente(cliente);
                mensagemSucesso(msg, "Cliente atualizado com sucesso!");
            } catch (SQLException e) {
                mensagemErro(msg, "Erro ao atualizar dados do cliente");
            }
        }
    }

    @Override
    public void limparForm() {
        this.nomeField.clear();
        this.sobrenomeField.clear();
        this.logradouroField.clear();
        this.numeroField.clear();
        this.complementoField.clear();
        this.bairroField.clear();
        this.cidadeField.clear();
        this.estadoBox.getSelectionModel().clearSelection();
        this.sexoBox.getSelectionModel().clearSelection();
    }

    @Override
    public void limparMensagens() {
        this.msg.setVisible(false);
        this.cpfMsg.setVisible(false);
    }

    @FXML
    public void ativarCampos() {
        this.nomeField.setDisable(false);
        this.sobrenomeField.setDisable(false);
        this.logradouroField.setDisable(false);
        this.numeroField.setDisable(false);
        this.complementoField.setDisable(false);
        this.bairroField.setDisable(false);
        this.cidadeField.setDisable(false);
        this.estadoBox.setDisable(false);
        this.sexoBox.setDisable(false);
    }
    
    @FXML
    public void desativarCampos() {
        this.nomeField.setDisable(true);
        this.sobrenomeField.setDisable(true);
        this.logradouroField.setDisable(true);
        this.numeroField.setDisable(true);
        this.complementoField.setDisable(true);
        this.bairroField.setDisable(true);
        this.cidadeField.setDisable(true);
        this.estadoBox.setDisable(true);
        this.sexoBox.setDisable(true);
    }

    
    
}
