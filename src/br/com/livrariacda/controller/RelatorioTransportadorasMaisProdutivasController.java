package br.com.livrariacda.controller;

import br.com.livrariacda.dao.RelatorioDAO;
import br.com.livrariacda.relatorio.TransportadoraMaisProdutiva;
import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.XYChart;

public class RelatorioTransportadorasMaisProdutivasController implements Initializable {

    @FXML
    public BarChart<Number,String> grafico;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.grafico.setTitle("Transportadoras Mais Produtivas");
        this.grafico.getXAxis().setLabel("Transportadora");
        this.grafico.getYAxis().setLabel("Quantidade de Entregas");

        try {
            List<TransportadoraMaisProdutiva> transportadoras = new RelatorioDAO().transportadorasMaisProdutivas();
            
            transportadoras.forEach((transportadora) -> {
                XYChart.Series series = new XYChart.Series();
                series.setName(transportadora.getNome());
                series.getData().add(new XYChart.Data(transportadora.getNome(), transportadora.getQuantidade()));
                this.grafico.getData().add(series);
            });

        } catch(SQLException e) {
            throw new RuntimeException(e);
        }
    }  
    
}
