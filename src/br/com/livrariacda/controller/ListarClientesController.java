package br.com.livrariacda.controller;

import br.com.livrariacda.dao.ClienteDAO;
import br.com.livrariacda.model.Cliente;
import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;


public class ListarClientesController implements Initializable {

    @FXML
    public TableView tabela;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ClienteDAO dao = new ClienteDAO();
        
        try {
            List<Cliente> clientes = dao.listarClientes();
            tabela.getItems().addAll(clientes);
            
            TableColumn<Cliente,String> cpfCol = new TableColumn<>("CPF");
            cpfCol.setCellValueFactory(new PropertyValueFactory<>("cpf"));
            tabela.getColumns().add(cpfCol);
            
            TableColumn<Cliente,String> nomeCol = new TableColumn<>("Nome");
            nomeCol.setCellValueFactory(new PropertyValueFactory<>("nome"));
            tabela.getColumns().add(nomeCol);
            
            TableColumn<Cliente,String> sobrenomeCol = new TableColumn<>("Sobrenome");
            sobrenomeCol.setCellValueFactory(new PropertyValueFactory<>("sobrenome"));
            tabela.getColumns().add(sobrenomeCol);
            
            TableColumn<Cliente,String> sexoCol = new TableColumn<>("Sexo");
            sexoCol.setCellValueFactory(new PropertyValueFactory<>("sexoString"));
            tabela.getColumns().add(sexoCol);
            
            TableColumn<Cliente,String> nascimentoCol = new TableColumn<>("Data de Nascimento");
            nascimentoCol.setCellValueFactory(new PropertyValueFactory<>("nascimentoFormatado"));
            tabela.getColumns().add(nascimentoCol);
            
            TableColumn<Cliente,String> logradouroCol = new TableColumn<>("Logradouro");
            logradouroCol.setCellValueFactory(new PropertyValueFactory<>("logradouro"));
            tabela.getColumns().add(logradouroCol);
            
            TableColumn<Cliente,String> numeroCol = new TableColumn<>("Numero");
            numeroCol.setCellValueFactory(new PropertyValueFactory<>("numero"));
            tabela.getColumns().add(numeroCol);
            
            TableColumn<Cliente,String> complementoCol = new TableColumn<>("Complemento");
            complementoCol.setCellValueFactory(new PropertyValueFactory<>("complemento"));
            tabela.getColumns().add(complementoCol);
            
            TableColumn<Cliente,String> bairroCol = new TableColumn<>("Bairro");
            bairroCol.setCellValueFactory(new PropertyValueFactory<>("bairro"));
            tabela.getColumns().add(bairroCol);
            
            TableColumn<Cliente,String> cidadeCol = new TableColumn<>("Cidade");
            cidadeCol.setCellValueFactory(new PropertyValueFactory<>("cidade"));
            tabela.getColumns().add(cidadeCol);
            
            TableColumn<Cliente,String> estadoCol = new TableColumn<>("Estado");
            estadoCol.setCellValueFactory(new PropertyValueFactory<>("estado"));
            tabela.getColumns().add(estadoCol);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    
}
