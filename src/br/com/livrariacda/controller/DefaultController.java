package br.com.livrariacda.controller;

import java.time.format.DateTimeFormatter;
import javafx.fxml.FXML;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

public abstract class DefaultController {
    
    @FXML
    public void mensagemErro(Text text, String mensagem) {
        text.setText(mensagem);
        text.setFill(Color.RED);
        text.setVisible(true);
    }
    
    @FXML
    public void mensagemSucesso(Text text, String mensagem) {
        text.setText(mensagem);
        text.setFill(Color.GREEN);
        text.setVisible(true);
    }
    
    public DateTimeFormatter getDateFormatter() {
        return DateTimeFormatter.ofPattern("yyyy-MM-dd");
    }
}
