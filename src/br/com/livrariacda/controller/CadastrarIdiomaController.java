package br.com.livrariacda.controller;

import br.com.livrariacda.dao.IdiomaDAO;
import br.com.livrariacda.model.Idioma;
import java.sql.SQLException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;


public class CadastrarIdiomaController extends FormController {

    @FXML
    private TextField idiomaField;
    
    @FXML
    private Text idiomaMsg;
    
    @FXML
    private void cadastrarIdioma(ActionEvent event) {
        this.limparMensagens();
        
        boolean erro = false;
        
        String nome = idiomaField.getText();
        if (nome.length() < 3) {
            this.mensagemErro(idiomaMsg, "Idioma invalido");
            erro = true;
        }
        
        if (!erro) {
            Idioma idioma = new Idioma();
            idioma.setNome(nome);
            try {
                new IdiomaDAO().cadastrarIdioma(idioma);
                
                this.limparForm();
                mensagemSucesso(msg, "Idioma cadastrado com sucesso!");   
            } catch (SQLException e) {
                mensagemErro(msg, "Erro ao cadastrar idioma");
            }
            
        }
    }

    @Override
    public void limparForm() {
        this.idiomaField.clear();
    }

    @Override
    public void limparMensagens() {
        idiomaMsg.setVisible(false);
        msg.setVisible(false);
    }
    
}
