package br.com.livrariacda.controller;

import br.com.livrariacda.dao.CompraDAO;
import br.com.livrariacda.model.Compra;
import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;


public class ListarComprasController extends FormController implements Initializable {
    
    @FXML
    public TextField cpfField;
    
    @FXML
    public Text cpfMsg;
    
    @FXML
    public TableView tabelaCompras;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        TableColumn<Compra,String> idCol = new TableColumn<>("ID");
        idCol.setCellValueFactory(new PropertyValueFactory<>("id"));
        tabelaCompras.getColumns().add(idCol);
        
        TableColumn<Compra,String> dataCol = new TableColumn<>("Data da Compra");
        dataCol.setCellValueFactory(new PropertyValueFactory<>("dataFormatada"));
        tabelaCompras.getColumns().add(dataCol);
    }
    
    @FXML
    public void buscarCompras(ActionEvent event) {
        boolean erro = false;
        
        long cpf = 0;
        try {
            if (cpfField.getText().length() != 11) {
                mensagemErro(cpfMsg, "CPF invalido");
                erro = true;
            }
            cpf = Long.parseLong(cpfField.getText());
        } catch (NumberFormatException e) {
            mensagemErro(cpfMsg, "CPF invalido");
            erro = true;
        }
        
        if (!erro) {
            try {
                List<Compra> compras = new CompraDAO().listarCompras(cpf);
                tabelaCompras.getItems().addAll(compras);
            } catch (SQLException e) {
                mensagemErro(msg, "Erro ao buscar compras");
            }
        }
    }

    @Override
    public void limparForm() {
        this.cpfField.clear();
    }

    @Override
    public void limparMensagens() {
        this.msg.setVisible(false);
        this.cpfMsg.setVisible(false);
    }
    
}
