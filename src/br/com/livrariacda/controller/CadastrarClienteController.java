package br.com.livrariacda.controller;

import br.com.livrariacda.dao.ClienteDAO;
import br.com.livrariacda.model.Cliente;
import java.sql.SQLException;
import java.time.LocalDate;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

public class CadastrarClienteController extends FormController {
    
    @FXML
    public TextField cpfField;
    
    @FXML
    public Text cpfMsg;

    @FXML
    public TextField nomeField;
    
    @FXML
    public Text nomeMsg;
    
    @FXML
    public TextField sobrenomeField;
    
    @FXML
    public Text sobrenomeMsg;
    
    @FXML
    public ComboBox sexoBox;
    
    @FXML
    public Text sexoMsg;
    
    @FXML
    public DatePicker nascimentoField;
    
    @FXML
    public Text nascimentoMsg;
    
    @FXML
    public TextField logradouroField;
    
    @FXML
    public Text logradouroMsg;
    
    @FXML
    public TextField numeroField;
    
    @FXML
    public Text numeroMsg;
    
    @FXML
    public TextField complementoField;
    
    @FXML
    public TextField bairroField;
    
    @FXML
    public Text bairroMsg;
    
    @FXML
    public TextField cidadeField;
    
    @FXML
    public Text cidadeMsg;
    
    @FXML
    public ComboBox estadoBox;
    
    @FXML
    public Text estadoMsg;
    
    @FXML
    public TextField dddField;
    
    @FXML
    public Text dddMsg;
    
    @FXML
    public TextField telefoneField;
    
    @FXML
    public Text telefoneMsg;
    
    @FXML
    public void cadastrarCliente(ActionEvent event) {
        this.limparMensagens();
        
        boolean erro = false;
        
        long cpf = 0;
        try {
            cpf = Long.parseLong(cpfField.getText());
            if (String.valueOf(cpf).length() != 11) {
                mensagemErro(cpfMsg, "CPF invalido");
                erro = true;
            }
        } catch (NumberFormatException e) {
            mensagemErro(cpfMsg, "CPF invalido");
            erro = true;
        }
        
        String nome = nomeField.getText();
        if (nome.length() < 3) {
            mensagemErro(nomeMsg, "Nome invalido");
            erro = true;
        }
        
        String sobrenome = sobrenomeField.getText();
        if (sobrenome.length() < 3) {
            mensagemErro(sobrenomeMsg, "Sobrenome invalido");
            erro = true;
        }
        
        int sexo = 0;
        try {
            String sexoString = sexoBox.getValue().toString();
            if (sexoString.equals("Feminino")) {
                sexo = 1;
            }
        } catch (Exception e) {
            mensagemErro(sexoMsg, "Sexo invalido");
            erro = true;
        }
        
        LocalDate nascimento = null;
        try {
            String nascimentoString = nascimentoField.getValue().toString();
            nascimento = LocalDate.parse(nascimentoString, this.getDateFormatter());
        } catch (Exception e) {
            mensagemErro(nascimentoMsg, "Data de Nascimento invalida");
            erro = true;
        }
        
        String logradouro = logradouroField.getText();
        if (logradouro.length() < 3) {
            mensagemErro(logradouroMsg, "Logradouro invalido");
        }
        
        int numero = 0;
        try {
            numero = Integer.parseInt(numeroField.getText());
        } catch (NumberFormatException e) {
            mensagemErro(numeroMsg, "Numero invalido");
            erro = true;
        }
        
        String complemento = complementoField.getText();
        
        String bairro = bairroField.getText();
        if (bairro.length() < 3) {
            mensagemErro(bairroMsg, "Bairro invalido");
            erro = true;
        }
        
        String cidade = cidadeField.getText();
        if (cidade.length() < 3) {
            mensagemErro(cidadeMsg, "Cidade invalido");
            erro = true;
        }
        
        String estado = new String();
        try{
            estado = estadoBox.getValue().toString();
            if (estado.length() != 2) {
                mensagemErro(estadoMsg, "Estado invalido");
                erro = true;
            }
        } catch (Exception e) {
            mensagemErro(estadoMsg, "Estado invalido");
            erro = true;
        }
        
        int ddd = 0;
        try {
            ddd = Integer.parseInt(dddField.getText());
            if (ddd < 0 || ddd > 99) {
                mensagemErro(dddMsg, "DDD invalido");
                erro = true;
            }
        } catch (NumberFormatException e) {
            mensagemErro(dddMsg, "DDD invalido");
            erro = true;
        }
        
        long telefone = 0;
        try {
            telefone = Long.parseLong(telefoneField.getText());
            if (telefone < 10000000 || telefone > 999999999) {
                mensagemErro(telefoneMsg, "Telefone invalido");
                erro = true;
            }
        } catch (NumberFormatException e) {
            mensagemErro(telefoneMsg, "Telefone invalido");
            erro = true;
        }
        
        if (!erro) {
            Cliente cliente = new Cliente();
            
            cliente.setCpf(cpf);
            cliente.setNome(nome);
            cliente.setSobrenome(sobrenome);
            cliente.setSexo(sexo);
            cliente.setNascimento(nascimento);
            cliente.setLogradouro(logradouro);
            cliente.setNumero(numero);
            cliente.setComplemento(complemento);
            cliente.setBairro(bairro);
            cliente.setCidade(cidade);
            cliente.setEstado(estado);
            
            ClienteDAO dao = new ClienteDAO();
            try {
                dao.cadastrarCliente(cliente);
                dao.cadastrarTelefone(cliente, ddd, telefone);
                
                this.limparForm();
                mensagemSucesso(msg, "Cliente cadastrado com sucesso!");  
            } catch (SQLException e) {
                mensagemErro(msg, "Erro ao cadastrar cliente");
            }
            
              
        }
    }
    
    @FXML
    @Override
    public void limparForm() {
        cpfField.clear();
        nomeField.clear();
        sobrenomeField.clear();
        sexoBox.getSelectionModel().clearSelection();
        nascimentoField.setValue(null);
        logradouroField.clear();
        numeroField.clear();
        complementoField.clear();
        bairroField.clear();
        cidadeField.clear();
        estadoBox.getSelectionModel().clearSelection();
        dddField.clear();
        telefoneField.clear();
    }
    
    @FXML
    @Override
    public void limparMensagens() {
        cpfMsg.setVisible(false);
        nomeMsg.setVisible(false);
        sobrenomeMsg.setVisible(false);
        sexoMsg.setVisible(false);
        nascimentoMsg.setVisible(false);
        logradouroMsg.setVisible(false);
        numeroMsg.setVisible(false);
        bairroMsg.setVisible(false);
        cidadeMsg.setVisible(false);
        estadoMsg.setVisible(false);
        dddMsg.setVisible(false);
        telefoneMsg.setVisible(false);
        msg.setVisible(false);
    }
    
}
