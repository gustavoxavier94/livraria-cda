package br.com.livrariacda.controller;

import br.com.livrariacda.dao.ClienteDAO;
import br.com.livrariacda.dao.CompraDAO;
import br.com.livrariacda.model.Cliente;
import br.com.livrariacda.model.Compra;
import br.com.livrariacda.model.Livro;
import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;

public class MostrarCompraController extends FormController implements Initializable {

    @FXML
    public TextField compraField;
    
    @FXML
    public Text dataCompra;
    
    @FXML
    public Text nomeCliente;
    
    @FXML
    public TableView tabelaLivros;
    
    @FXML
    public Text valorTotal;
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        TableColumn<Livro,String> isbnCol = new TableColumn<>("ISBN");
        isbnCol.setCellValueFactory(new PropertyValueFactory<>("isbn"));
        tabelaLivros.getColumns().add(isbnCol);

        TableColumn<Livro,String> tituloCol = new TableColumn<>("Titulo");
        tituloCol.setCellValueFactory(new PropertyValueFactory<>("titulo"));
        tabelaLivros.getColumns().add(tituloCol);

        TableColumn<Livro,String> lancamentoCol = new TableColumn<>("Data de Lancamento");
        lancamentoCol.setCellValueFactory(new PropertyValueFactory<>("lancamentoFormatado"));
        tabelaLivros.getColumns().add(lancamentoCol);

        TableColumn<Livro,String> edicaoCol = new TableColumn<>("Edicao");
        edicaoCol.setCellValueFactory(new PropertyValueFactory<>("edicao"));
        tabelaLivros.getColumns().add(edicaoCol);

        TableColumn<Livro,String> idiomaCol = new TableColumn<>("Idioma");
        idiomaCol.setCellValueFactory(new PropertyValueFactory<>("idioma"));
        tabelaLivros.getColumns().add(idiomaCol);

        TableColumn<Livro,String> valorCol = new TableColumn<>("Valor (R$)");
        valorCol.setCellValueFactory(new PropertyValueFactory<>("valorFormatado"));
        tabelaLivros.getColumns().add(valorCol);

        TableColumn<Livro,String> paginasCol = new TableColumn<>("Paginas");
        paginasCol.setCellValueFactory(new PropertyValueFactory<>("paginas"));
        tabelaLivros.getColumns().add(paginasCol);

        TableColumn<Livro,String> classificacaoCol = new TableColumn<>("Classificacao");
        classificacaoCol.setCellValueFactory(new PropertyValueFactory<>("classificacao"));
        tabelaLivros.getColumns().add(classificacaoCol);

        TableColumn<Livro,String> editoraCol = new TableColumn<>("Editora");
        editoraCol.setCellValueFactory(new PropertyValueFactory<>("editora"));
        tabelaLivros.getColumns().add(editoraCol);
    }
    
    
    @FXML
    public void mostrarCompra(ActionEvent event) {
        this.limparMensagens();
        this.limparForm();
        
        boolean erro = false;
        
        Compra compra = new Compra();
        try {
            int id = Integer.parseInt(compraField.getText());
            compra = new CompraDAO().buscarCompra(id);
            if (compra.getId() == 0) {
                mensagemErro(msg, "Compra nao encontrada");
                erro = true;
            }
        } catch (NumberFormatException e) {
            mensagemErro(msg, "Codigo da compra invalido");
            erro = true;
        } catch (SQLException e) {
            mensagemErro(msg, "Erro ao buscar compra");
            erro = true;
        }
        
        if (!erro) {
            try { 
                Cliente cliente = new ClienteDAO().buscarCliente(compra.getCliente());
                List<Livro> livros = new CompraDAO().livrosCompra(compra.getId());
                
                this.dataCompra.setText(compra.getDataFormatada());
                this.nomeCliente.setText(cliente.getNomeCompleto());
                this.tabelaLivros.getItems().addAll(livros);
                
                double total = 0.0;
                
                for (Livro livro : livros) {
                    total += livro.getValor();
                }
                
                this.valorTotal.setText("R$ " + String.format("%10.2f", total).replace('.', ','));
            } catch (SQLException e) {
                mensagemErro(msg, "Erro ao buscar dados da compra");
            }
        }
    }

    @Override
    public void limparForm() {
        this.dataCompra.setText(null);
        this.nomeCliente.setText(null);
        this.tabelaLivros.getItems().removeAll(tabelaLivros.getItems());
        this.valorTotal.setText(null);
    }

    @Override
    public void limparMensagens() {
        this.msg.setVisible(false);
    }

    
}
