package br.com.livrariacda.controller;

import br.com.livrariacda.dao.RelatorioDAO;
import br.com.livrariacda.relatorio.ClienteQueMaisComprou;
import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.XYChart;

public class RelatorioClientesQueMaisCompraramController implements Initializable {

    @FXML
    public BarChart<Number,String> grafico;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.grafico.setTitle("Clientes Que Mais Compraram");
        this.grafico.getXAxis().setLabel("Cliente");
        this.grafico.getYAxis().setLabel("Quantidade de Livros");

        try {
            List<ClienteQueMaisComprou> clientes = new RelatorioDAO().clientesQueMaisCompraram();
            
            clientes.forEach((cliente) -> {
                XYChart.Series series = new XYChart.Series();
                series.setName(cliente.getCliente());
                series.getData().add(new XYChart.Data(cliente.getCliente(), cliente.getQuantidade()));
                this.grafico.getData().add(series);
            });

        } catch(SQLException e) {
            throw new RuntimeException(e);
        }
    }    
    
}
