package br.com.livrariacda.controller;

import br.com.livrariacda.dao.FuncionarioDAO;
import br.com.livrariacda.model.Funcionario;
import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

public class ListarFuncionariosController implements Initializable {
    
    @FXML
    TableView<Funcionario> tabela;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        FuncionarioDAO dao = new FuncionarioDAO();
        
        try {
            List<Funcionario> funcionarios = dao.getFuncionarios();
            tabela.getItems().addAll(funcionarios);
            
            TableColumn<Funcionario,String> matriculaCol = new TableColumn<>("Matricula");
            matriculaCol.setCellValueFactory(new PropertyValueFactory<>("matricula"));
            tabela.getColumns().add(matriculaCol);
            
            TableColumn<Funcionario,String> nomeCol = new TableColumn<>("Nome");
            nomeCol.setCellValueFactory(new PropertyValueFactory<>("nomeCompleto"));
            tabela.getColumns().add(nomeCol);
            
            TableColumn<Funcionario,String> sexoCol = new TableColumn<>("Sexo");
            sexoCol.setCellValueFactory(new PropertyValueFactory<>("sexoString"));
            tabela.getColumns().add(sexoCol);
            
            TableColumn<Funcionario,String> cargoCol = new TableColumn<>("Cargo");
            cargoCol.setCellValueFactory(new PropertyValueFactory<>("cargo"));
            tabela.getColumns().add(cargoCol);
            
            TableColumn<Funcionario,String> logradouroCol = new TableColumn<>("Logradouro");
            logradouroCol.setCellValueFactory(new PropertyValueFactory<>("logradouro"));
            tabela.getColumns().add(logradouroCol);
            
            TableColumn<Funcionario,String> numeroCol = new TableColumn<>("Numero");
            numeroCol.setCellValueFactory(new PropertyValueFactory<>("numero"));
            tabela.getColumns().add(numeroCol);
            
            TableColumn<Funcionario,String> complementoCol = new TableColumn<>("Complemento");
            complementoCol.setCellValueFactory(new PropertyValueFactory<>("complemento"));
            tabela.getColumns().add(complementoCol);
            
            TableColumn<Funcionario,String> bairroCol = new TableColumn<>("Bairro");
            bairroCol.setCellValueFactory(new PropertyValueFactory<>("bairro"));
            tabela.getColumns().add(bairroCol);
            
            TableColumn<Funcionario,String> cidadeCol = new TableColumn<>("Cidade");
            cidadeCol.setCellValueFactory(new PropertyValueFactory<>("cidade"));
            tabela.getColumns().add(cidadeCol);
            
            TableColumn<Funcionario,String> estadoCol = new TableColumn<>("Estado");
            estadoCol.setCellValueFactory(new PropertyValueFactory<>("estado"));
            tabela.getColumns().add(estadoCol);
            
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    
    
}
