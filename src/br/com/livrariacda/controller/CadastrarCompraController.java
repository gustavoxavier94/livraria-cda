package br.com.livrariacda.controller;

import br.com.livrariacda.dao.CaixaDAO;
import br.com.livrariacda.dao.ClienteDAO;
import br.com.livrariacda.dao.CompraDAO;
import br.com.livrariacda.dao.FormaPagamentoDAO;
import br.com.livrariacda.dao.LivroDAO;
import br.com.livrariacda.dao.VendedorDAO;
import br.com.livrariacda.model.Caixa;
import br.com.livrariacda.model.Cliente;
import br.com.livrariacda.model.Compra;
import br.com.livrariacda.model.FormaPagamento;
import br.com.livrariacda.model.Funcionario;
import br.com.livrariacda.model.Livro;
import br.com.livrariacda.model.Vendedor;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;


public class CadastrarCompraController extends FormController implements Initializable {
    
    @FXML
    public TextField cpfField;
    
    @FXML
    public Text cpfMsg;
    
    @FXML
    public ComboBox formaPagamentoBox;
    
    @FXML
    public Text formaPagamentoMsg;
    
    @FXML
    public TableView livroTable;
    
    @FXML
    public TextField isbnField;
    
    @FXML
    public ComboBox vendedorBox;
    
    @FXML
    public Text vendedorMsg;
    
    @FXML
    public ComboBox caixaBox;
    
    @FXML
    public Text caixaMsg;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        TableColumn<Livro,String> isbnCol = new TableColumn<>("ISBN");
        isbnCol.setCellValueFactory(new PropertyValueFactory<>("isbn"));
        livroTable.getColumns().add(isbnCol);
        
        TableColumn<Livro,String> tituloCol = new TableColumn<>("Titulo");
        tituloCol.setCellValueFactory(new PropertyValueFactory<>("titulo"));
        livroTable.getColumns().add(tituloCol);
        
        TableColumn<Livro,String> valorCol = new TableColumn<>("Valor (R$)");
        valorCol.setCellValueFactory(new PropertyValueFactory<>("valorFormatado"));
        livroTable.getColumns().add(valorCol);
        
        List<FormaPagamento> formasPagamento = new ArrayList<>();
        List<Funcionario> vendedores = new ArrayList<>();
        List<Funcionario> caixas = new ArrayList<>();
        
        try {
            formasPagamento = new FormaPagamentoDAO().listarFormasPagamento();
            caixas = new CaixaDAO().listarCaixas();
            vendedores = new VendedorDAO().listarVendedores();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        
        this.formaPagamentoBox.getItems().addAll(formasPagamento);
        this.caixaBox.getItems().addAll(caixas);
        this.vendedorBox.getItems().addAll(vendedores);
    }
    
    @FXML
    public void adicionarLivro(ActionEvent event) {
        this.msg.setVisible(false);
        
        try {
            
            long isbn = Long.parseLong(isbnField.getText());
            Livro livro = new LivroDAO().buscarLivro(isbn);
            
            if (livro.getIsbn() == 0) {
                mensagemErro(msg, "Livro nao encontrado");
            } else {
                livroTable.getItems().add(livro);
            }
            
        } catch (NumberFormatException e) {
            
            mensagemErro(msg, "Codigo ISBN invalido");
            
        } catch (SQLException e) {
            
            throw new RuntimeException(e);            
        }
    }
    
    @FXML
    public void cadastrarCompra(ActionEvent event) {
        this.limparMensagens();
        boolean erro = false;
        
        Cliente cliente = new Cliente();
        try {
            long cpf = Long.parseLong(cpfField.getText());
            if (String.valueOf(cpf).length() != 11) {
                mensagemErro(cpfMsg, "CPF deve conter 11 digitos");
                erro = true;
            }
            cliente = new ClienteDAO().buscarCliente(cpf);
            if (cliente.getCpf() == 0) {
                mensagemErro(cpfMsg, "Cliente nao encontrado");
                erro = true;
            }
            
        } catch (NumberFormatException e) {
            mensagemErro(cpfMsg, "CPF invalido");
            erro = true;
        } catch (SQLException e) {
            mensagemErro(cpfMsg, "Cliente nao encontrado");
            erro = true;
        }
        
        FormaPagamento formaPagamento = new FormaPagamento();
        try {
            formaPagamento = (FormaPagamento) formaPagamentoBox.getValue();
            if (formaPagamento == null) {
                mensagemErro(formaPagamentoMsg, "Forna de pagamento invalida");
                erro = true;
            }
        } catch (Exception e) {
            mensagemErro(formaPagamentoMsg, "Forma de pagamento invalida");
            erro = true;
        }
        
        List<Livro> livros = new ArrayList<>(livroTable.getItems());
        if (livros.isEmpty()) {
            mensagemErro(msg, "Nenhum livro foi incluido na compra");
            erro = true;
        }
        
        Vendedor vendedor = new Vendedor();
        try {
            vendedor = (Vendedor) vendedorBox.getValue();
            if (vendedor == null) {
                mensagemErro(vendedorMsg, "Vendedor invalido");
                erro = true;
            }
        } catch (Exception e) {
            mensagemErro(vendedorMsg, "Vendedor invalido");
            erro = true;
        }
        
        Caixa caixa = new Caixa();
        try {
            caixa = (Caixa) caixaBox.getValue();
            if (caixa == null) {
                mensagemErro(caixaMsg, "Caixa invalido");
                erro = true;
            }
        } catch (Exception e) {
            mensagemErro(caixaMsg, "Caixa invalido");
            erro = true;
        }
        
        if (!erro) {
            Compra compra = new Compra();
            compra.setCliente(cliente.getCpf());
            compra.setData(LocalDate.now());
            compra.setFormaDePagamento(formaPagamento.getId());
            
            CompraDAO dao = new CompraDAO();
            try {
                dao.cadastrarCompra(compra);
                dao.inserirProcessa(compra, caixa);
                dao.inserirParticipa(compra, vendedor);
                dao.inserirContem(compra, livros);
                
                this.limparForm();
                this.mensagemSucesso(msg, "Compra cadastrada com sucesso! Codigo da compra: " + compra.getId());
            } catch (SQLException e) {
                e.printStackTrace();
                mensagemErro(msg, "Erro ao cadastrar compra");
            }
            
        }
    }

    @Override
    public void limparForm() {
        this.cpfField.clear();
        this.formaPagamentoBox.getSelectionModel().clearSelection();
        this.livroTable.setItems(null);
        this.isbnField.clear();
        this.vendedorBox.getSelectionModel().clearSelection();
        this.caixaBox.getSelectionModel().clearSelection();
    }

    @Override
    public void limparMensagens() {
        this.cpfMsg.setVisible(false);
        this.formaPagamentoMsg.setVisible(false);
        this.vendedorMsg.setVisible(false);
        this.caixaMsg.setVisible(false);
        this.msg.setVisible(false);
    }
    
}
