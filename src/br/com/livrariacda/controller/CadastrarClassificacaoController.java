package br.com.livrariacda.controller;

import br.com.livrariacda.dao.ClassificacaoDAO;
import br.com.livrariacda.model.Classificacao;
import java.sql.SQLException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

public class CadastrarClassificacaoController extends FormController {

    @FXML
    public TextField classificacaoField;
    
    @FXML
    public Text classificacaoMsg;
    
    @FXML
    public void cadastrarClassificacao(ActionEvent event) {
        this.limparMensagens();
        
        boolean erro = false;
        
        String nome = classificacaoField.getText();
        if (nome.length() < 3) {
            this.mensagemErro(classificacaoMsg, "Classificacao invalida");
            erro = true;
        }
        
        if (!erro) {
            Classificacao classificacao = new Classificacao();
            classificacao.setNome(nome);
            
            ClassificacaoDAO dao = new ClassificacaoDAO();
            try {
                dao.cadastrarClasificacao(classificacao);
            } catch (SQLException e) {
                mensagemErro(msg, "Erro ao cadastrar classificacao");
            }
            this.limparForm();
            mensagemSucesso(msg, "Classificacao cadastrada com sucesso!");    
        }
    }

    @Override
    public void limparForm() {
        this.classificacaoField.clear();
    }

    @Override
    public void limparMensagens() {
        classificacaoMsg.setVisible(false);
        msg.setVisible(false);
    }
    

}
