package br.com.livrariacda.controller;

import br.com.livrariacda.dao.EditoraDAO;
import br.com.livrariacda.model.Editora;
import java.sql.SQLException;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

public class CadastrarEditoraController extends FormController {
    
    @FXML
    public TextField cnpjField;
    
    @FXML
    public Text cnpjMsg;
    
    @FXML
    public TextField nomeField;
    
    @FXML
    public Text nomeMsg;
    
    @FXML
    public void cadastrarEditora() {
        this.limparMensagens();
        boolean erro = false;
        
        long cnpj = 0;
        try {
            cnpj = Long.parseLong(cnpjField.getText());
            if (String.valueOf(cnpj).length() != 14) {
                mensagemErro(cnpjMsg, "CNPJ invalido");
                erro = true;
            }
        } catch (NumberFormatException e) {
            mensagemErro(cnpjMsg, "CNPJ invalido");
            erro = true;
        }
        
        String nome = nomeField.getText();
        if (nome.length() < 3) {
            mensagemErro(nomeMsg, "Nome invalido");
            erro = true;
        }
        
        if (!erro) {
            Editora editora = new Editora();
            
            editora.setCnpj(cnpj);
            editora.setNome(nome);
            
            try {
                new EditoraDAO().cadastrarEditora(editora);
                this.limparForm();
                mensagemSucesso(msg, "Editora cadastrada com sucesso!");
            } catch (SQLException e) {
                mensagemErro(msg, "Erro ao cadastrar Editora");
            }

        }
    }

    @Override
    public void limparForm() {
        cnpjField.clear();
        nomeField.clear();
    }

    @Override
    public void limparMensagens() {
        cnpjMsg.setVisible(false);
        nomeMsg.setVisible(false);
        msg.setVisible(false);
    }
    
}
