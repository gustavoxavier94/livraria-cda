package br.com.livrariacda.controller;

import br.com.livrariacda.dao.RelatorioDAO;
import br.com.livrariacda.relatorio.CompraPorClassificacao;
import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.PieChart;


public class RelatorioComprasPorClassificacaoController implements Initializable {

    @FXML
    public PieChart grafico;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.grafico.setTitle("Compras por Classificacao");
        
        try {
            List<CompraPorClassificacao> compras = new RelatorioDAO().comprasPorCategoria();
            
            ObservableList<PieChart.Data> pieChartData = FXCollections.observableArrayList();
            
            compras.forEach((cliente) -> {
               pieChartData.add(new PieChart.Data(cliente.getClassificacao(), cliente.getQuantidade()));
            });
            
            grafico.getData().addAll(pieChartData);

        } catch(SQLException e) {
            throw new RuntimeException(e);
        }
        
        
    }       
    
}
