package br.com.livrariacda.controller;

import br.com.livrariacda.dao.RelatorioDAO;
import br.com.livrariacda.relatorio.EditoraMaisAtiva;
import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.XYChart;

public class RelatorioEditorasMaisAtivasController implements Initializable {

    @FXML
    public BarChart<Number,String> grafico;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.grafico.setTitle("Editoras Mais Ativas");
        this.grafico.getXAxis().setLabel("Editora");
        this.grafico.getYAxis().setLabel("Quantidade de Livros");

        try {
            List<EditoraMaisAtiva> editoras = new RelatorioDAO().editorasMaisAtivas();
            
            editoras.forEach((editora) -> {
                XYChart.Series series = new XYChart.Series();
                series.setName(editora.getNome());
                series.getData().add(new XYChart.Data(editora.getNome(), editora.getQuantidade()));
                this.grafico.getData().add(series);
            });

        } catch(SQLException e) {
            throw new RuntimeException(e);
        }
    }      
    
}
